package com.secaglobal.twitter.mesh.common.domains.impl;

import com.secaglobal.twitter.mesh.common.domains.PullUserTweetsJob;

/**
 * Twitter tweet representation
 *
 * <p>
 *     This class represents twitter tweet entity class
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public class DefaultPullUserTweetsJob implements PullUserTweetsJob {
    public Long userId;
    public String twitterAccessToken;
    public String twitterAccessTokenSecret;
    public Iterable<String> words;

    /**
     * Returns job owner id
     * @return job owner id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Sets job owner id
     * @param userId job owner id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }


    /**
     * Returns access token for twitter client account
     * @return access token
     */
    public String getTwitterAccessToken() {
        return twitterAccessToken;
    }

    /**
     * Sets access token for twitter client account
     * @param twitterAccessToken access token
     */
    public void setTwitterAccessToken(String twitterAccessToken) {
        this.twitterAccessToken = twitterAccessToken;
    }

    /**
     * Twitter access token secret key
     * @return access token secret key
     */
    public String getTwitterAccessTokenSecret() {
        return twitterAccessTokenSecret;
    }

    /**
     * Twitter access token secret key
     * @param twitterAccessTokenSecret secret key
     */
    public void setTwitterAccessTokenSecret(String twitterAccessTokenSecret) {
        this.twitterAccessTokenSecret = twitterAccessTokenSecret;
    }

    /**
     * List of words specified by client for filtering
     * @return list of words
     */
    public Iterable<String> getWords() {
        return words;
    }

    /**
     * Sets list of words for client message filtering
     * @param words list of words
     */
    public void setWords(Iterable<String> words) {
        this.words = words;
    }
}
