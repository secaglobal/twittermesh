package com.secaglobal.twitter.mesh.common.controllers.responses.impl;

import com.secaglobal.twitter.mesh.common.controllers.internal.response.data.agent.JobsData;

/**
 * Jobbs data response implementation
 *
 * <p>
 *      This class implements response for Jobs Data
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class JobsDataResponse extends DefaultResponse<JobsData> {}
