package com.secaglobal.twitter.mesh.common.messages.impl;

import com.secaglobal.twitter.mesh.common.messages.Message;

/**
 * Simple message implementation
 *
 * <p>
 *      This class realize the most common and simple functional for messaging.
 *      This class just works like wrapper for message.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class SimpleMessage implements Message {
    private String message;

    public SimpleMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
