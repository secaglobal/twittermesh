package com.secaglobal.twitter.mesh.common.controllers.internal.response.data.agent;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;
import com.secaglobal.twitter.mesh.common.domains.Job;

/**
 * Jobs list response data
 *
 * <p>
 *      This class implements holder for Agent jobs
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 03.02.2015
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="@type")
public class JobsData extends AbstractData {
    public Job[] jobs;

    public JobsData() {}
    public JobsData(Job[] jobs) {
        this.jobs = jobs;
    }
}
