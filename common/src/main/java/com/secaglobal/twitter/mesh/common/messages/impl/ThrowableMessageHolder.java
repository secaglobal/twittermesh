package com.secaglobal.twitter.mesh.common.messages.impl;

import com.secaglobal.twitter.mesh.common.messages.Message;
import com.secaglobal.twitter.mesh.common.messages.MessageHolder;

import java.util.List;

/**
 * Holder for messages. Can be used as exception
 *
 * <p>This class provides holder for messages. It is throwable</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 24.01.2015
 */
public class ThrowableMessageHolder extends Throwable implements MessageHolder {
    private List<Message> messages;

    public ThrowableMessageHolder(List<Message> messages) {
        this.messages = messages;
    }

    /**
     * Returns list of messages provided by sender
     */
    public List<Message> getMessages() {
        return messages;
    }
}
