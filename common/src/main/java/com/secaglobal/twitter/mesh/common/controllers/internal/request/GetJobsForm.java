package com.secaglobal.twitter.mesh.common.controllers.internal.request;

/**
 * Describes get jobs form
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 03.02.2015
 */
public class GetJobsForm {
    private int limit;

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}


