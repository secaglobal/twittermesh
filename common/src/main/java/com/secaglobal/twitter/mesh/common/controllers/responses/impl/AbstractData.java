package com.secaglobal.twitter.mesh.common.controllers.responses.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.secaglobal.twitter.mesh.common.controllers.responses.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abstract Response Data
 *
 * <p>
 *      This class realize common methods for Response Data classes
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 30.01.2015
 */
public class AbstractData implements Data {
    static private final Logger logger = LoggerFactory.getLogger(AbstractData.class);

    public Boolean __data__ = true;

    public String toString() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);

            return new StringBuilder(this.getClass().getSimpleName())
                    .append(" ")
                    .append(mapper.writeValueAsString(this))
                    .toString();
        } catch (JsonProcessingException e) {
            logger.warn("Cannot serialize object of class: " + this.getClass().getName());
            return super.toString();
        }
    }
}
