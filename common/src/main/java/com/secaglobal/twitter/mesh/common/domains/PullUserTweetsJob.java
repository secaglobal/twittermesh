package com.secaglobal.twitter.mesh.common.domains;

/**
 * Pull user tweets job representation
 *
 * <p>
 *     This class is interface that describes job for pulling user tweets
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 05.02.2015
 */
public interface PullUserTweetsJob extends Job {

    /**
     * Returns job owner id
     * @return job owner id
     */
    Long getUserId();

    /**
     * Sets job owner id
     * @param username job owner id
     */
    void setUserId(Long username);

    /**
     * Returns access token for twitter client account
     * @return access token
     */
    String getTwitterAccessToken();

    /**
     * Sets access token for twitter client account
     * @param twitterAccessToken access token
     */
    void setTwitterAccessToken(String twitterAccessToken);

    /**
     * Twitter access token secret key
     * @return access token secret key
     */
    String getTwitterAccessTokenSecret();

    /**
     * Twitter access token secret key
     * @param twitterAccessTokenSecret secret key
     */
    void setTwitterAccessTokenSecret(String twitterAccessTokenSecret);

    /**
     * List of words specified by client for filtering
     * @return list of words
     */
    Iterable<String> getWords();

    /**
     * Sets list of words for client message filtering
     * @param words list of words
     */
    void setWords(Iterable<String> words);
}
