package com.secaglobal.twitter.mesh.common.controllers.responses;

/**
 * Describes data representation
 *
 * <p>This interface uses to mark class as data that can be returned as response data</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface Data {}
