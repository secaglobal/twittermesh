package com.secaglobal.twitter.mesh.common.domains.impl;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;

import java.util.Date;

/**
 * Twitter tweet representation
 *
 * <p>
 *     This class represents twitter tweet entity class
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public class DefaultTwitterTweet implements TwitterTweet {
    private Long vendorId;
    private String message;
    private String authorName;
    private Date postedDate;
    private Long userId;

    /**
     * Returns twitter unique identifier
     * @return tweet unique identifier
     */
    public Long getVendorId() {
        return vendorId;
    }

    /**
     * Sets twitter unique identifier
     * @param id twitter unique identifier
     */
    public void setVendorId(Long id) {
        this.vendorId = id;
    }

    /**
     * Returns tweet message body
     * @return message body
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets tweet message body
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Returns tweet author name
     * @return author name
     */
    public String getAuthor() {
        return this.authorName;
    }

    /**
     * Sets tweet author name
     * @param authorName name of tweet author
     */
    public void setAuthor(String authorName) {
        this.authorName = authorName;
    }

    /**
     * Returns the date when tweet was posted
     * @return the date when tweet was posted
     */
    public Date getPostedDate() {
        return postedDate;
    }

    /**
     * Sets the date when tweet was posted
     * @param date the date when tweet was posted
     */
    public void setPostedDate(Date date) {
        this.postedDate = date;
    }

    /**
     * Returns user id (owner id)
     * @return user id
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Sets user id (owner id)
     * @param userId user id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
