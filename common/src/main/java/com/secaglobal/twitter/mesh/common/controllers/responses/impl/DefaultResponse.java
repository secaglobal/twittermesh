package com.secaglobal.twitter.mesh.common.controllers.responses.impl;

import com.secaglobal.twitter.mesh.common.controllers.responses.Response;
import com.secaglobal.twitter.mesh.common.messages.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * Default response implementation
 *
 * <p>
 *      This class realize the most common and simple functionality for response.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class DefaultResponse<D extends Object> implements Response<DefaultResponse, D> {
    private D data;
    private List<Message> errors = new ArrayList<>();

    /**
     * Returns response data
     * @return
     */
    public D getData() {
        return data;
    }

    /**
     * Sets response data
     * @param data controller answer
     */
    public DefaultResponse<D> setData(D data) {
        this.data = data;
        return this;
    }

    /**
     * Returns response errors
     * @return
     */
    public List<Message> getErrors() {
        return errors;
    }

    /**
     * Sets response errors
     * @param errors Errors ocured in time of request
     */
    public DefaultResponse<D> setErrors(List<Message> errors) {
        this.errors = errors;
        return this;
    }

    /**
     * Sets response errors
     * @returns status
     *
     * TODO UnitTest
     */
    public boolean isSuccessful() {
        return this.errors.isEmpty();
    }
}
