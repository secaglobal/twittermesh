package com.secaglobal.twitter.mesh.common.messages;

/**
 * Describes response messaging
 *
 * <p>This interface describes error representation, that can be passed as response messaging</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 24.01.2015
 */
public interface Message {
    /**
     * Returns error
     * @return
     */
    String getMessage();
}
