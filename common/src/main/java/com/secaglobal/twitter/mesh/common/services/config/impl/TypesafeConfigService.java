package com.secaglobal.twitter.mesh.common.services.config.impl;

import com.secaglobal.twitter.mesh.common.services.config.ConfigService;
import com.typesafe.config.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.Properties;

/**
 * Implements {@link com.secaglobal.twitter.mesh.common.services.config.ConfigService @ConfigService}
 *
 * <p>
 *     Actually it is wrapper for {@link com.typesafe.config.ConfigFactory @com.typesafe.config.ConfigFactory}
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 * @see com.secaglobal.twitter.mesh.common.services.config.ConfigService
 * @see com.typesafe.config.ConfigFactory
 */
public class TypesafeConfigService implements ConfigService {
    private final Logger logger = LoggerFactory.getLogger(TypesafeConfigService.class);
    private Config config;

    public TypesafeConfigService() {
        logger.info("Configuration initialization");

        config =  ConfigFactory.load();
    }

    /**
     * Merge additional config file
     * <p>This method load specified configuration file and merge it with current configuration</p>
     * @param path Path to file. Looking at classpath
     * @see com.typesafe.config.ConfigFactory#load(com.typesafe.config.Config)
     * @see com.typesafe.config.Config#withFallback(com.typesafe.config.ConfigMergeable)
     *
     * TODO UnitTest
     */
    @Override
    public void setConfigLocation(String path) {
        logger.info("Load configuration file: {}", path);

        config = ConfigFactory.load(path).withFallback(config);
    }

    /**
     * Merge additional config files
     * <p>This method load specified configuration files and merge it with current configuration</p>
     * @param paths Paths to files. Looking at classpath
     * @see TypesafeConfigService#setConfigLocation
     *
     * TODO UnitTest
     */
    @Override
    public void setConfigLocationList(String... paths) {
        for (String path: paths) {
            setConfigLocation(path);
        }
    }

    /**
     * Convert config implementation to properties
     * <p>Convert {@link com.typesafe.config.Config @Config} implementation into {@link java.util.Properties @Properties}.
     * Useful if concrete class / service supports only Properties<p/>
     * @return Config as Properties
     *
     * TODO UnitTest
     */
    @Override
    public Properties asProperties() {
        Properties props =  new Properties();

        logger.trace("Conversion config to properties using asProperties");

        for (Map.Entry<String, ConfigValue> entry: config.entrySet()) {
            if (logger.isTraceEnabled()) {
                logger.trace("Property: {} = {}", entry.getKey(), entry.getValue().unwrapped().toString());
            }

            props.setProperty(entry.getKey(), entry.getValue().unwrapped().toString());
        }

        return props;
    }

    /**
     * Return property
     * <p>This method extract property value and returns it as required type<p/>
     * @param name
     * @param <T> Type of required property
     * @return Required property. Do not check property type and required type.
     *
     * TODO UnitTest
     */
    @Override
    @SuppressWarnings("unchecked")
    public <T> T getProperty(String name) {
        return (T) config.getValue(name).unwrapped();
    }

    /**
     * Add property to config
     * @param name Property name
     * @param value Value
     * @param <T> Type of required property.
     *
     * TODO UnitTest
     */
    @Override
    public <T> void setProperty(String name, final T value) {
        logger.info("Set custom config property: {} = ", name, value);

        config = config.withValue(name, new Value() {
            @Override
            public Object unwrapped() {
                return value;
            }
        });
    }

    private abstract class Value implements ConfigValue {
        @Override
        public ConfigOrigin origin() {
            return null;
        }

        @Override
        public ConfigValueType valueType() {
            return null;
        }

        @Override
        public String render() {
            return null;
        }

        @Override
        public String render(ConfigRenderOptions options) {
            return null;
        }

        @Override
        public ConfigValue withFallback(ConfigMergeable other) {
            return null;
        }

        @Override
        public Config atPath(String path) {
            return null;
        }

        @Override
        public Config atKey(String key) {
            return null;
        }
    }
}
