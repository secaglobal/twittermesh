package com.secaglobal.twitter.mesh.common.controllers.internal.request;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;

import java.util.LinkedList;

/**
 * Describes form refreshing tweets. Just for internal use.
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 14.02.2015
 */
public class RefreshTweetsForm {
    private Tweets tweets;

    public Tweets getTweets() {
        return tweets;
    }

    public void setTweets(Tweets tweets) {
        this.tweets = tweets;
    }

    public static class Tweets extends LinkedList<TwitterTweet> {

    }
}


