package com.secaglobal.twitter.mesh.common.messages;

import java.util.List;

/**
 * Holder for messages.
 *
 * <p>This interface describes holder for messages</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 24.01.2015
 */
public interface MessageHolder {

    /**
     * Returns list of messages provided by sender
     */
    public List<Message> getMessages();
}
