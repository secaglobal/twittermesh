package com.secaglobal.twitter.mesh.common.controllers.responses;

import com.secaglobal.twitter.mesh.common.messages.Message;

import java.util.List;

/**
 * Describes response
 *
 * <p>This interface provides api for controller responses</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface Response<R extends Response, D extends Object> {
    /**
     * Returns response data
     * @return
     */
    D getData();

    /**
     * Returns response errors
     * @return
     */
    List<Message> getErrors();

    /**
     * Find out if request successful
     */
    boolean isSuccessful();
}
