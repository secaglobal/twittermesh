define(['App'], function(App) {
  App.provider('RouteAuth', [function() {
    var defaultParams = {
      access : 'forbiddenForAll',
      redirectTo : '/'
    };

    var conditionals =  {
      permitAll: true,
      forbiddenForAll: false
    };

    RouteAuthProvider.$get = function() {return RouteAuthProvider; }

    RouteAuthProvider.addConditional = function(name, value) {
      conditionals[name] = value;
    };

    return RouteAuthProvider;

    function RouteAuthProvider(params) {
      var conf = angular.extend({}, defaultParams, params);
      var rule;

      return ['$parse', '$log', '$rootScope', '$location', '$q', function($parse, $log, $rootScope, $location, $q) {
        var deferred = $q.defer();

        $log.debug("Resolving rout auth: ", conf);

        rule = rule || $parse(conf.access);

        if (rule($rootScope, conditionals)) {
          $log.debug("Access was granted");
          deferred.resolve();
        } else {
          $log.debug("Access was not granted. Redirecting to: ", conf.redirectTo);
          $location.path(conf.redirectTo);
          deferred.reject();
        }

        return deferred.promise;
      }]
    };
  }]);
});