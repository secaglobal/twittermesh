requirejs.config({
  baseUrl: 'assets/app',
  shim: {
    angularResource: ['angular'],
    angularRoute: ['angular'],
    bootstrap: ['jquery']
  },
  paths: {
    App: './app',
    angular: '/assets/bower_components/angular/angular',
    angularResource: '/assets/bower_components/angular-resource/angular-resource',
    angularRoute: '/assets/bower_components/angular-route/angular-route',
    bootstrap: '/assets/bower_components/bootstrap/dist/js/bootstrap',
    jquery: '/assets/bower_components/jquery/dist/jquery',
    text: '/assets/bower_components/requirejs-text/text',
    json: '/assets/bower_components/requirejs-json/json',
    env: '/main-app/system/env'
  }
});

// Start the main app logic.
requirejs([
  'text',
  'json',
  'App',
  'bootstrap',
  'config',
  'auth',
  'route',
  'controllers/user/signup/signup',
  'controllers/user/login/login',
  'controllers/user/logout/logout',
  'controllers/tweet/list/list'
], function() {
    angular.bootstrap(document, ['twitter.mesh']);
});