define(['App'], function(App) {
  App.controller("TweetListController", [
    '$scope', '$http', '$interval', '$sce', 'API_URL__MESSAGE_WORDS_LIST', 'API_URL__MESSAGE_WORDS_SAVE', 'API_URL__TWEET_LIST',
    function($scope, $http, $interval, $sce, API_URL__MESSAGE_WORDS_LIST, API_URL__MESSAGE_WORDS_SAVE, API_URL__TWEET_LIST) {
      var words = [];

      $scope.wordsSync = true;
      $scope.tweetsSync = true;
      $scope.isUpToDate = false;
      $scope.words = '';
      $scope.updateWords = updatedWords;
      $scope.wrapWords = wrapWords;

      refreshWords();
      refreshTweets();

      var interval = $interval(refreshTweets, 15000);

      $scope.$on('$destroy', function() {
        $interval.cancel(interval);
      });

      function refreshTweets() {
        $scope.tweetsSync = true;

        $http.get(API_URL__TWEET_LIST, {}).success(function(data) {
          if (data.successful) {
            $scope.tweets = data.data.tweets;
            $scope.isUpToDate = data.data.isUpToDate;
          }

          $scope.tweetsSync = false;
        });
      }

      function refreshWords() {
        $scope.wordsSync = true;

        $http.get(API_URL__MESSAGE_WORDS_LIST, {}).success(function(data) {
          if (data.successful) {
            words = data.data.words;
            $scope.words = words.join(' ');
          }

          $scope.wordsSync = false;
        });
      }

      function updatedWords() {
        words = this.words.split(/[,;.\s]+/).filter(function(d) { return d.length; });
        $scope.words = words.join(' ');
        $scope.wordsSync = true;

        $http.post(API_URL__MESSAGE_WORDS_SAVE, {words: words}).finally(function() {
          $scope.wordsSync = false;
          $scope.config.$setPristine();
          refreshTweets();
        });
      }

      function wrapWords(message) {
         if (!words || !words.length) return $sce.trustAsHtml(message);
         return $sce.trustAsHtml(message.replace(new RegExp('(' + words.join("|") + ')',"gi"), '<span class="label-success">$1</span>'));
      }
  }]);
});