<div class="col-sm-offset-1 col-sm-10">
    <h3 class="page-header">Tweets filter</h3>
    <form name="config" ng-submit="false">
        <div class="form-group" ng-class="{'has-success':config.words.$dirty}">
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-search"></span>
                <input class="form-control" ng-disabled="wordsSync" placeholder="Enter filter" name="words" type="text" ng-model="words" ng-model-options="{updateOn: 'default'}"/>
                <div class="input-group-btn">
                    <button class="btn btn-default" ng-class="{disabled:config.words.$pristine}" ng-disabled="wordsSync" ng-click="updateWords()"><i class="glyphicon glyphicon-ok" ></i></button>
                </div>
            </div>
        </div>
    </form>

    <div class="alert alert-warning" ng-show="!wordsSync && !words.length">Please specify filter before use</div>

    <h3 class="page-header">Tweets</h3>
    <div class="alert alert-info" ng-show="!isUpToDate && !wordsSync && words.length">Refreshing tweets list! Please wait...</div>

    <div class="alert alert-warning" ng-show="isUpToDate && !(tweets && tweets.length)">
        No tweets found
    </div>

    <ul class="list-group">
        <li class="list-group-item" ng-repeat="tweet in tweets">
            <span class="badge">{{tweet.author}}</span>
            <span ng-bind-html="wrapWords(tweet.message)"></span>
        </li>
    </ul>
</div>
