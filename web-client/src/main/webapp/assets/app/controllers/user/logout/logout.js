define(['App'], function(App) {
  App.controller("UserLogoutController", [
    '$route', '$http', '$location', 'SystemEnvService', 'API_URL__LOGOUT',
    function($route, $http, $location, SystemEnvService, API_URL__LOGOUT) {

    $http.get(API_URL__LOGOUT)
      .success(reload)
      .error(reload);

    function reload() {
      SystemEnvService.refresh().$promise.then(function() {
        $route.reload();
      });

    }
  }]);
});