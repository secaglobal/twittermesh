define(['App'], function(App) {
  App.controller("UserLoginController",
    ['$scope', '$http', 'SystemEnvService', 'API_URL__LOGIN', '$route',
      function($scope, $http, SystemEnvService, API_URL__LOGIN, $route) {

    $scope.submit = submit.bind(null, $scope, $http);

    function submit($scope, $http) {
      var form = $scope.loginForm;

      if (!form.$valid) return;

      $http.post(API_URL__LOGIN, {
        username: form.username.$viewValue,
        password: form.password.$viewValue
      }).success(function(data) {
        this.errors = angular.isArray(data.errors) && data.errors.length && data.errors;

        if (data.successful) {
          SystemEnvService.refresh().$promise.then($route.reload.bind($route));
        }
      }.bind($scope));
    }
  }]);
});