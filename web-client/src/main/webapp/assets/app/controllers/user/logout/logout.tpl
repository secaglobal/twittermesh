<div class="col-sm-offset-1 col-sm-10">
    <h3 class="page-header">Logout</h3>
    <div class="alert alert-info">Logging out. Wait for a couple of seconds</div>
</div>
