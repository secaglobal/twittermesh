<div class="col-sm-offset-1 col-sm-10">
    <h3 class="page-header">Welcome to Twitter Mesh</h3>
    <h5 class="margin-left-30">
        <span class="glyphicon glyphicon-menu-right margin-left-minus-30 pull-left">&nbsp;</span>
        Twitter Mesh is a service for analyzing a text message streams.
    </h5>
    <h5 class="margin-left-30">
        <span class="glyphicon glyphicon-menu-right margin-left-minus-30 pull-left">&nbsp;</span>
        With Twitter Mesh clusterization of events became <b>very easy</b>, <b>fast</b>, <b>helpful</b> and <b>enjoyable</b>!
    </h5>

    <h3 class="page-header">Log in</h3>
    <h5 ng-repeat="error in errors">- {{error.message}}</h5>
    <form name="loginForm" novalidate ng-submit="false">
        <div ng-show="loginForm.$submitted || loginForm.email.$touched">
            <ul>
             <li ng-show="loginForm.email.$error.required">Email is required</li>
             <li ng-show="loginForm.email.$error.email">This is not a valid email</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-user"></span>
                <input class="form-control" placeholder="email" name="username" type="email" ng-model="username" required />
            </div>
        </div>

        <div ng-show="loginForm.$submitted || loginForm.password.$touched">
            <ul>
                <li ng-show="loginForm.password.$error.required">Password is required</li>
                <li ng-show="loginForm.password.$error.minlength">Password should be 6 character length at least</li>
                <li ng-show="loginForm.password.$error.maxlength">Password should be 255 character length the nost</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-lock"></span>
                <input class="form-control" placeholder="password" name="password" type="password" ng-model="password" required minlength="6" maxlength="255" />
            </div>
        </div>
        <button class="btn btn-default pull-right" ng-click="submit()" >Submit</button>
    </form>
</div>
