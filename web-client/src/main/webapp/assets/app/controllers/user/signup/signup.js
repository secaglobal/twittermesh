define(['App'], function(App) {
  App.controller("UserSignupController", ['$scope', '$http', 'SystemEnvService', 'API_URL__SIGNUP', function($scope, $http, SystemEnvService, API_URL__SIGNUP) {
    $scope.submit = submit.bind(null, $scope, $http);

    function submit($scope, $http) {
      var form = $scope.signupForm;

      if (!form.$valid) return;

      $http.post(API_URL__SIGNUP, {
        username: form.username.$viewValue,
        password: form.password.$viewValue
      }).success(function(data) {
        this.errors = angular.isArray(data.errors) && data.errors.length && data.errors;

        if (data.successful) {
          SystemEnvService.refresh();
        }
      }.bind($scope));
    }
  }]);
});