<div class="col-sm-offset-1 col-sm-10">
    <h3 class="page-header">Sign up</h3>
    <h5 ng-repeat="error in errors">- {{error.message}}</h5>
    <form name="signupForm" novalidate ng-submit="false">
        <div ng-show="signupForm.$submitted || signupForm.email.$touched">
            <ul>
             <li ng-show="signupForm.email.$error.required">Email is required</li>
             <li ng-show="signupForm.email.$error.email">This is not a valid email</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-user"></span>
                <input class="form-control" placeholder="email" name="username" type="email" ng-model="username" required />
            </div>
        </div>

        <div ng-show="signupForm.$submitted || signupForm.password.$touched">
            <ul>
                <li ng-show="signupForm.password.$error.required">Password is required</li>
                <li ng-show="signupForm.password.$error.minlength">Password should be 6 character length at least</li>
                <li ng-show="signupForm.password.$error.maxlength">Password should be 255 character length the nost</li>
            </ul>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon glyphicon glyphicon-lock"></span>
                <input class="form-control" placeholder="password" name="password" type="password" ng-model="password" required minlength="6" maxlength="255" />
            </div>
        </div>
        <button class="btn btn-default pull-right" ng-click="submit()" >Submit</button>
    </form>
</div>
