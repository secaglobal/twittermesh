define(['App', 'auth'], function(App) {
  App.config(['$routeProvider', 'RouteAuthProvider', function($routeProvider, Authentication) {
    $routeProvider
      .when("/signup", {
        templateUrl: 'assets/app/controllers/user/signup/signup.tpl',
        controller: 'UserSignupController',
        resolve: {
          auth: Authentication({access: '!authenticated()', redirectTo: '/'})
        }
      })

      .when("/login", {
        templateUrl: 'assets/app/controllers/user/login/login.tpl',
        controller: 'UserLoginController',
        resolve: {
          auth: Authentication({access: '!authenticated()', redirectTo: '/'})
        }
      })

      .when("/tweets", {
        templateUrl: 'assets/app/controllers/tweet/list/list.tpl',
        controller: 'TweetListController',
        resolve: {
          auth: Authentication({access: 'authenticated()', redirectTo: '/login'})
        }
      })

      .when("/logout", {
        templateUrl: 'assets/app/controllers/user/logout/logout.tpl',
        controller: 'UserLogoutController',
        resolve: {
          auth: Authentication({access: 'authenticated()', redirectTo: '/login'})
        }
      })

      .otherwise("/tweets");
  }]);
});