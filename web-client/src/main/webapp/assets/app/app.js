define(['angular', 'angularRoute', 'angularResource'], function() {
  return angular.module('twitter.mesh', ['ngRoute', 'ngResource']);
});