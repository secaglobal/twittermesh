define(['App'], function(App) {
  App.factory('SystemEnv', ['API_URL', '$resource', function(API_URL, $resource) {
    return $resource(API_URL + '/system/env', {}, {
      get: {method:'GET'}
    });
  }]);
});