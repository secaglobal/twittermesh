define(['App', 'json!env', 'services/system/env-service'], function(App, env) {
  App.constant("API_URL", "/main-app");
  App.constant("API_URL__LOGIN", "/main-app/user/login");
  App.constant("API_URL__SIGNUP", "/main-app/user/signup");
  App.constant("API_URL__LOGOUT", "/main-app/logout");
  App.constant("API_URL__MESSAGE_WORDS_LIST", "/main-app/message-word");
  App.constant("API_URL__MESSAGE_WORDS_SAVE", "/main-app/message-word");
  App.constant("API_URL__TWEET_LIST", "/main-app/tweet");

  App.run(['SystemEnvService', function(SystemEnvService) {
    SystemEnvService.load(env.data);
  }])
});