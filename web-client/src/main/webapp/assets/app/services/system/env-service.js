define(['App', 'resources/system/env'], function(App) {
  App.service('SystemEnvService', ['SystemEnv', '$rootScope', '$log', 'API_URL', function(SystemEnv, $rootScope, $log, API_URL) {
    return {
      refresh:  refresh,
      check: checkTwitterAccess,
      load: load
    };

    function load(env) {
      $log.debug('SystemEnv: Loading new system env', env);
      $rootScope.env = env;
      this.check();
    }

    function refresh() {
      $log.info('SystemEnv: Getting env');

      return SystemEnv.get(function(data) {
        if (angular.isObject(data) && data['successful']) {
          this.load(data.data);
        } else {
          $log.error('SystemEnv: Got broken response', data);
          this.refresh();
        }
      }.bind(this));
    }

    function checkTwitterAccess() {
      $log.info('SystemEnv: Checking twitter access');
      var env = $rootScope.env;

      if (env.authenticated && !env.twitterAccessGranted) {
        $log.debug('SystemEnv: Twitter access is not granted. Redirection to : ' + API_URL + '/twitter/auth-request');
        document.location.href = API_URL + '/twitter/auth-request?redirectTo=' + encodeURIComponent(document.location.toString());
      }
    }
  }]);
});