define(['App', 'config', 'providers/route-auth-provider', 'services/system/env-service'], function(App) {

  App.run(['RouteAuth', '$rootScope', function(RouteAuth, $rootScope) {
    RouteAuth.addConditional('authenticated', function() {
      var env = $rootScope.env;
      return !env || env.authenticated;
    });
  }]);
});