package com.secaglobal.twitter.mesh.main.application.controllers.web.request.twitter;

import com.secaglobal.twitter.mesh.main.application.controllers.web.request.user.CredentialsForm;
import org.springframework.validation.annotation.Validated;

/**
 * Describes auth callback form
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
@Validated
public class AuthCallbackForm extends CredentialsForm {
    private String redirectTo = "/";
    private String oauth_verifier = "";

    public String getRedirectTo() {
        return redirectTo;
    }

    public void setRedirectTo(String redirectTo) {
        this.redirectTo = redirectTo;
    }

    public String getOauth_verifier() {
        return oauth_verifier;
    }

    public void setOauth_verifier(String oauth_verifier) {
        this.oauth_verifier = oauth_verifier;
    }
}


