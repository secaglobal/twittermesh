package com.secaglobal.twitter.mesh.main.application.controllers.web.request.twitter;

import com.secaglobal.twitter.mesh.main.application.controllers.web.request.user.CredentialsForm;
import org.springframework.validation.annotation.Validated;

/**
 * Describes auth request form
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
@Validated
public class AuthRequestForm extends CredentialsForm {
    private String redirectTo = "/";

    public String getRedirectTo() {
        return redirectTo;
    }

    public void setRedirectTo(String redirectTo) {
        this.redirectTo = redirectTo;
    }
}


