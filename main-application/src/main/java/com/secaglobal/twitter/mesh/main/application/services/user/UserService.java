package com.secaglobal.twitter.mesh.main.application.services.user;

import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.common.messages.impl.ThrowableMessageHolder;
import com.secaglobal.twitter.mesh.main.application.repositories.UserRepository;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.social.oauth1.OAuthToken;

import java.util.Date;
import java.util.Set;

/**
 * Interface provides api for h User Service
 *
 * <p>This interface describes api for business logic related to User domain</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 25.01.2015
 */
public interface UserService {

    /**
     * Register new user
     * @return user
     * @throws com.secaglobal.twitter.mesh.common.messages.impl.ThrowableMessageHolder
     */
    User registerUser(String username, String password) throws ThrowableMessageHolder;

    /**
     * Login user
     * @return user
     */
    boolean loginUser(String username, String password, WebAuthenticationDetails details);

    /**
     * Find out if user complete Twitter setup
     * @return user
     */
    boolean isTwitterSetupCompleted(User user);

    /**
     * Sets oauth twitter token for user
     * @param user user
     * @param token Twitter Access token (OAuth1)
     */
    void setTwitterAccessToken(User user, OAuthToken token);

    /**
     * Sets date when tweets request was done by user.
     * @param user user
     */
    void updateTweetRequestDate(User user);

    /**
     * Reset flag that tweets is up to date. So client shouldn't use them
     * @param user user
     */
    void resetTweetsUpToDateFlag(User user);

    /**
     * Sets flag that tweets is up to date and can be used by user
     * @param user user
     */
    void setTweetsUpToDateFlag(User user);

    /**
     * Sets flag that tweets is up to date and can be used by user
     * @param userIds users ids
     */
    void setTweetsUpToDateFlagById(Iterable<Long> userIds);

    /**
     * Returns user authorized in current security context
     * @return user
     */
    User getCurrentUser();

    /**
     * Returns list of words that should be used for message filtering
     * @param user user
     * @return words List of new words
     */
    Iterable<? extends MessageWord> getMessageWords(User user);

    /**
     * Specify witch words should be used for message filtering
     * This method should reset current user tweets.
     * @param user user
     * @param words List of new words
     */
    void assignMessageWords(User user, Set<String> words);

    /**
     * Returns list of users, that should be processed next by Agent
     * @param limit
     * @return
     */
    Iterable<User> getUsersToMessageUpdate(int limit);

    /**
     * Returns user repository api
     * @return
     */
    UserRepository getRepository();
}
