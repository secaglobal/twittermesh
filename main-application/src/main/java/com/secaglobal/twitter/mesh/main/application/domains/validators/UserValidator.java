package com.secaglobal.twitter.mesh.main.application.domains.validators;

import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.repositories.UserRepository;
import com.secaglobal.twitter.mesh.main.application.utils.ValidationUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 *  User validation
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class UserValidator implements Validator {
    private UserRepository repository;

    public UserValidator(UserRepository repository) {
        this.repository = repository;
    }

    /**
     * Checks if supplied class allowed for validation
     * @param clazz Class
     * @return status
     */
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    /**
     * Validate target. Place errors into Errors collection
     *
     * Todo UnitTest
     */
    public void validate(Object target, Errors errors) {
        User user = (User) target;


        validateUsername(user, errors);
        validatePassword(user, errors);
        validateTwitterAccessToken(user, errors);


        if (!errors.hasErrors() && user.getId() == null) {
            validateExistance(user, errors);
        }
    }

    private void validatePassword(User user, Errors errors) {
        String passwordHash = user.getPassword();

        if (passwordHash == null ||
            !ValidationUtils.isEqual(passwordHash.length(), 32)
        ) {
            errors.rejectValue("password", "system.common.incorrectly_hashed_pass");
        }
    }

    private void validateUsername(User user, Errors errors) {
        String username = user.getUsername();

        if (username == null ||
            !ValidationUtils.isValidEmail(username)
        ) {
            errors.rejectValue("email", "common.email.invalid_email");
        }
    }

    private void validateTwitterAccessToken(User user, Errors errors) {
        String token = user.getTwitterAccessToken();

        if (token != null &&
            ValidationUtils.isGreaterOrEqual(token.length(), 256)
        ) {
            errors.rejectValue("twitterAccessToken", "system.twitter.access_token.too_long");
        }
    }

    private void validateExistance(User user, Errors errors) {
        User availableUser = repository.findByUsername(user.getUsername());

        if (availableUser != null) {
            errors.reject("user.already_exist");
        }
    }
}
