package com.secaglobal.twitter.mesh.main.application.domains.impl;

import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * Entity User
 */
@Entity
@Table(name = "MessageWord")
public class JpaMessageWord implements MessageWord {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 20)
    private String word;

    @Column(nullable = false)
    private Long userId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    @JoinColumn(name = "userId", insertable = false, updatable = false)
    private JpaUser user;

    /**
     * Returns user id (unique)
     * @return identifuer
     */
    public Long getId() {
        return id;
    }

    /**
     * Set user id
     * @param id unique identifier
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns word
     * @return word
     */
    public String getWord() {
        return word;
    }

    /**
     * Set word
     * @param word word
     */
    public void setWord(String word) {
        this.word = word;
    }

    /**
     * Returns user id that word is assigned to
     * @return user id (owner id)
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * Set user id that word is assigned to
     * @param userId user id (owner id)
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Returns user that word is assigned to
     * @return user (owner)
     */
    public User getUser() {
        return this.user;
    }

    /**
     * Set user that word is assigned to
     * @param user user (owner)
     */
    public void setUser(JpaUser user) {
        this.user = user;
    }
}
