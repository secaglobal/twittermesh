package com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.messageWord;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;

/**
 * Response on message words saving
 *
 * <p>
 *      This class implements holder for answer on saving message words defined by user for searching.
 *      Uses usually by web client
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 02.02.2015
 */
public class WordsSavedData extends AbstractData {}
