package com.secaglobal.twitter.mesh.main.application.repositories;

import com.secaglobal.twitter.mesh.main.application.domains.Entity;

/**
 * Default repository interface
 *
 * <p>This interface describes default api for interaction with data instances</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface Repository<T extends Entity, ID extends java.io.Serializable> extends org.springframework.data.repository.Repository<T, ID> {

    /**
     * Creates new instance. But do not persist it.
     * @return new instance
     */
    T create();

    /**
     * Persist instances
     * @param instances instances
     */
    <S extends T> Iterable<S> save(Iterable<S> instances);

    /**
     * Persist instance
     * @param instance instance
     * @return same instance
     */
    <S extends T> S save(S instance);

    /**
     * Load instance by id
     * @param id id
     * @return same instance
     */
    T findById(Long id);

    /**
     * Delete item
     * @param instane instance
     */
    void delete(T instane);

    /**
     * Delete list of items
     * @param instances
     */
    void delete(java.lang.Iterable<? extends T> instances);

    /**
     * Returns list of all available instances
     * @return list of instances
     */
    java.lang.Iterable<T> findAll();
}