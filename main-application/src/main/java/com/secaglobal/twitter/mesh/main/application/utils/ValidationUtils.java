package com.secaglobal.twitter.mesh.main.application.utils;

import org.apache.commons.validator.routines.EmailValidator;

/**
 * Set of method useful for validation
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 *
 * TODO UnitTest
 */
public class ValidationUtils {
    public static boolean isValidEmail(String email) {
        return EmailValidator.getInstance().isValid(email);
    }

    public static boolean isLess(long value, long expected) {
        return value < expected;
    }

    public static boolean isLessOrEqual(long value, long expected) {
        return value <= expected;
    }

    public static boolean isGreater(long value, long expected) {
        return value > expected;
    }

    public static boolean isGreaterOrEqual(long value, long expected) {
        return value >= expected;
    }

    public static boolean isEqual(long value, long expected) {
        return value == expected;
    }
}
