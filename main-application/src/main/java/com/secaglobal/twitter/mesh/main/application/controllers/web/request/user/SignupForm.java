package com.secaglobal.twitter.mesh.main.application.controllers.web.request.user;

import org.springframework.validation.annotation.Validated;

/**
 * Describes signup form
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
public class SignupForm extends CredentialsForm {}


