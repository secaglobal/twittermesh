package com.secaglobal.twitter.mesh.main.application.services.twitter.impl;

import com.secaglobal.twitter.mesh.main.application.services.twitter.TwitterOAuth1Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.oauth1.AuthorizedRequestToken;
import org.springframework.social.oauth1.OAuth1Operations;
import org.springframework.social.oauth1.OAuth1Parameters;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

/**
 * Service for interaction with Twitter OAuth1 authentication
 *
 * <p>This service provides api for OAuth authentication provided by Twitter</p>
 *
 * TODO UnitTest
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class DefaultTwitterOAuth1Service implements TwitterOAuth1Service {
    private static final Logger logger = LoggerFactory.getLogger(DefaultTwitterOAuth1Service.class);
    private TwitterConnectionFactory connectionFactory;

    public DefaultTwitterOAuth1Service(TwitterConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    /**
     * Prepares representation of request token.
     * Uses for authorization and access token verification
     * @param callbackUrl url that receives and processes Twitter access token
     * @return
     * @see DefaultTwitterOAuth1Service#getAuthorizeUrl
     * @see DefaultTwitterOAuth1Service#varifyAuthorization
     */
    public OAuthToken createRequestToken(String callbackUrl) {
        logger.debug("Creation of twitter access token: callbackUrl = {}", callbackUrl);

        OAuth1Operations operations = connectionFactory.getOAuthOperations();
        OAuthToken token = operations.fetchRequestToken(callbackUrl, null);

        logger.debug("Created twitter access token {}", token.getValue());
        logger.trace("Created twitter access token secret key", token.getSecret());

        return token;
    }

    /**
     * Prepares Twitter authorization url
     * @return url
     */
    public String getAuthorizeUrl(OAuthToken requestToken) {
        logger.debug("Building authorization url: token = {}", requestToken.getValue());

        OAuth1Operations operations = connectionFactory.getOAuthOperations();
        String url = operations.buildAuthorizeUrl(requestToken.getValue(), OAuth1Parameters.NONE);

        logger.debug("Created twitter authorize url ", url);

        return url;
    }

    /**
     * Varify received twitter access token
     */
    public OAuthToken varifyAuthorization(OAuthToken requestToken, String oauthVerifier) {
        logger.debug("Exchanging access token: token = {}, verifier", requestToken.getValue(), oauthVerifier);

        OAuth1Operations operations = connectionFactory.getOAuthOperations();

        OAuthToken authorizedAccessToken =  operations.exchangeForAccessToken(
                new AuthorizedRequestToken(requestToken, oauthVerifier), null);

        logger.debug("Received authorized twitter access token {}", authorizedAccessToken.getValue());
        logger.trace("Received authorized twitter access token secret key", authorizedAccessToken.getSecret());

        return authorizedAccessToken;
    }
}
