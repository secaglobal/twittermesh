package com.secaglobal.twitter.mesh.main.application.controllers.internal;

import com.secaglobal.twitter.mesh.common.controllers.internal.response.data.agent.JobsData;
import com.secaglobal.twitter.mesh.common.controllers.responses.impl.DefaultResponse;
import com.secaglobal.twitter.mesh.common.controllers.responses.impl.JobsDataResponse;
import com.secaglobal.twitter.mesh.common.domains.Job;
import com.secaglobal.twitter.mesh.common.domains.impl.DefaultPullUserTweetsJob;
import com.secaglobal.twitter.mesh.main.application.controllers.internal.request.twitterTweet.RefreshTweetsForm;
import com.secaglobal.twitter.mesh.main.application.controllers.internal.responses.agent.TweetsSavedData;
import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.services.twitter.TweetService;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;

/**
 * Handles Agent specific requests
 *
 * <p>This class provides rest api for requests related to Agent<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
@RestController
@RequestMapping("/internal/agent")
public class AgentController {
    private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

    @Resource(name = "userService")
    UserService userService;

    @Resource(name = "tweetService")
    private TweetService tweetService;

    /**
     * Prepare list of Agent jobs
     * Do not provided checking if user is authorized
     * @return response
     */
    @RequestMapping(value="/jobs", method = RequestMethod.GET)
    public DefaultResponse<JobsData> jobs() {
        logger.info("Agent requested jobs for processing");

        Iterable<User> users =  userService.getRepository().findAllWaitingTweetsUpdate();
        List<Job> jobs = new LinkedList<>();

        for (User user: users) {
            DefaultPullUserTweetsJob job = new DefaultPullUserTweetsJob();
            List<String> words = new LinkedList<>();

            job.userId = user.getId();
            job.twitterAccessToken = user.getTwitterAccessToken();
            job.twitterAccessTokenSecret = user.getTwitterAccessTokenSecret();

            for (MessageWord word: userService.getMessageWords(user)) {
                words.add(word.getWord());
            }

            job.words = words;
            jobs.add(job);
        }

        Job[] jobsArr = jobs.toArray(new Job[jobs.size()]);

        return new JobsDataResponse().setData(new JobsData(jobsArr));
    }

    @RequestMapping(value="/tweet", method = RequestMethod.POST)
    public DefaultResponse<TweetsSavedData> refreshTweets(@RequestBody RefreshTweetsForm form) {
        logger.info("Received new tweets for clients");
        tweetService.saveNewCollectedTweets(form.getTweets());
        return new DefaultResponse<TweetsSavedData>().setData(new TweetsSavedData());
    }
}
