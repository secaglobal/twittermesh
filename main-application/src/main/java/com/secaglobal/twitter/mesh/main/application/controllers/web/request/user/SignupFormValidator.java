package com.secaglobal.twitter.mesh.main.application.controllers.web.request.user;

/**
 * Provides validation of signup form
 *
 * <p>This class provides methods for validation signup form received from web client<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class SignupFormValidator extends CredentialsFormValidator {

    /**
     * Checks if supplied class allowed for validation
     * @param clazz Class
     * @return status
     */
    public boolean supports(Class<?> clazz) {
        return SignupForm.class.isAssignableFrom(clazz);
    }
}
