package com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.twitterTweet;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;
import com.secaglobal.twitter.mesh.main.application.domains.TwitterTweet;

/**
 * List of client tweets
 *
 * <p>
 *      This class implements holder for tweets assigned to client.
 *      Uses usually by web client
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 13.02.2015
 */
public class AbstractTweetsListData extends AbstractData {
    public Iterable<TwitterTweet> tweets;
    public Boolean isUpToDate;
}
