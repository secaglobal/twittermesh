package com.secaglobal.twitter.mesh.main.application.repositories.impl;

import com.secaglobal.twitter.mesh.main.application.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.main.application.domains.impl.JpaTwitterTweet;
import com.secaglobal.twitter.mesh.main.application.repositories.TwitterTweetRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * Repository for interaction of users message words
 *
 * <p>This class describes interface for interaction with MessageWord instances using JPA approach</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 *
 * TODO UnitTest
 */
public class JpaTwitterTweetRepository extends AbstractJpaRepository<TwitterTweet> implements TwitterTweetRepository {

    /**
     * Constructor
     */
    public JpaTwitterTweetRepository() {
        super(JpaTwitterTweet.class);
    }

    /**
     * Returns assigned to user
     * @params userId user id (identifier
     * @return user instance
     */
    @Transactional
    @SuppressWarnings("unchecked")
    public List<TwitterTweet> findAllByUserId(Long userId) {
        List words =  entityManager
                .createQuery("select u from JpaTwitterTweet u where u.userId = :userId")
                .setParameter("userId", userId)
                .getResultList();

        return words;
    }

    /**
     * Removes tweets assigned to user
     * @params userId user id (identifier)
     * @return user instance
     */
    @Transactional
    public void deleteAllByUserId(Long userId) {
        deleteAllByUserId(Arrays.asList(userId));
    }

    /**
     * Removes tweets assigned to user
     * @params userIds user ids (identifier)
     * @return user instance
     */
    @Transactional
    public void deleteAllByUserId(Iterable<Long> userIds) {
        entityManager
                .createQuery("delete from JpaTwitterTweet u where u.userId in (:userIds)")
                .setParameter("userIds", userIds)
                .executeUpdate();

    }
}