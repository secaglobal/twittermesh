package com.secaglobal.twitter.mesh.main.application.filters;

import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Stub Filter
 *
 * <p>
 *      This class is filter that do nothing.
 *      Can be used in cases where filter is required
 * <p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 28.01.2015
 */
public class NoneFilter implements Filter {

    /**
     * Initialize request
     * @param filterConfig configuration
     * @throws javax.servlet.ServletException
     */
    public void init(FilterConfig filterConfig) throws ServletException {}


    /**
     * Do nothing
     * @param request Servlet request
     * @param response Servlet response
     * @param chain Filters chain. Needs to prolong chain traversing
     * @throws java.io.IOException
     * @throws javax.servlet.ServletException
     */
    public void doFilter ( ServletRequest request, ServletResponse response, FilterChain chain )
            throws IOException, ServletException {

        chain.doFilter(request, response);
    }

    /**
     * Filter destructor
     */
    public void destroy() {}

}
