package com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.user;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;

/**
 * Login response data
 *
 * <p>
 *      This class implements holder for login response data.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
public class LoginData extends AbstractData {}
