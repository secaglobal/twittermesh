package com.secaglobal.twitter.mesh.main.application.domains;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.secaglobal.twitter.mesh.main.application.domains.impl.JpaTwitterTweet;

import java.util.Date;

/**
 * Twitter tweet representation
 *
 * <p>
 *     This class represents twitter tweet entity class
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "@type")
@JsonSubTypes(@JsonSubTypes.Type(value = JpaTwitterTweet.class, name = "tweet"))
public interface TwitterTweet extends Entity {
    /**
     * Returns twitter unique identifier
     * @return tweet unique identifier
     */
    Long getId();

    /**
     * Sets twitter unique identifier
     * @param id twitter unique identifier
     */
    void setId(Long id);

    /**
     * Returns vendor unique identifier
     * @return tweet unique identifier
     */
    Long getVendorId();

    /**
     * Sets vendor unique identifier
     * @param id twitter unique identifier
     */
    void setVendorId(Long id);

    /**
     * Returns tweet message body
     * @return message body
     */
    String getMessage();

    /**
     * Sets tweet message body
     * @param message
     */
    void setMessage(String message);

    /**
     * Returns tweet author name
     * @return author name
     */
    String getAuthor();

    /**
     * Sets tweet author name
     * @param authorName name of tweet author
     */
    void setAuthor(String authorName);

    /**
     * Returns the date when tweet was posted
     * @return the date when tweet was posted
     */
    Date getPostedDate();

    /**
     * Sets the date when tweet was posted
     * @param date the date when tweet was posted
     */
    void setPostedDate(Date date);

    /**
     * Returns user id (owner id)
     * @return user id
     */
    Long getUserId();

    /**
     * Sets user id (owner id)
     * @param userId user id
     */
    void setUserId(Long userId);
}
