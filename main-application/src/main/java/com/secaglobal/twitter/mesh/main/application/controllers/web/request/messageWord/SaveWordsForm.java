package com.secaglobal.twitter.mesh.main.application.controllers.web.request.messageWord;

import java.util.Set;

/**
 * Describes credentials form
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
public class SaveWordsForm {
    private Set<String> words;

    public Set<String> getWords() {
        return words;
    }

    public void setWords(Set<String> words) {
        this.words = words;
    }
}


