package com.secaglobal.twitter.mesh.main.application.domains;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Date;
import java.util.Set;

/**
 * User Domain Interface
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface User extends Entity, UserDetails {

    /**
     * Returns username (unique)
     * @return username
     */
    String getUsername();

    /**
     * Set user username
     * @param username username
     */
    void setUsername(String username);

    /**
     * Returns password (hashed)
     * @return hashed password
     */
    String getPassword();

    /**
     * Password (hashed)
     * @param password hashed password
     */
    void setPassword(String password);

    /**
     * Returns twitter access token
     * @return token
     */
    String getTwitterAccessToken();

    /**
     * Sets Twitter token
     * @param token Valid twitter access token
     */
    void setTwitterAccessToken(String token);

    /**
     * Returns twitter
     * @return token
     */
    String getTwitterAccessTokenSecret();

    /**
     * Sets Twitter token secret
     * @param tokenSecret Valid twitter access token secret
     */
    void setTwitterAccessTokenSecret(String tokenSecret);

    /**
     * Returns registration date
     * @return date
     */
    Date getCreatedAt();

    /**
     * Sets registration date
     * @param date date
     */
    void setCreatedAt(Date date);

    /**
     * Figure out if current tweets is up to date
     * @return state
     */
    Boolean isTweetsUpToDate();

    /**
     * Sets if current tweets is up to date
     * @param state current state
     */
    void setTweetsUpToDate(Boolean state);

    /**
     * Returns date when tweets was requested last
     * This date shows how long ago client request tweets.
     * It is necessary for organizing tweets refreshing
     * @return date
     */
    Date getLastTweetRequestAt();

    /**
     * Sets date when tweets was requested last
     * This date shows how long ago client request tweets.
     * It is necessary for organizing tweets refreshing
     * @param date date
     */
    void setLastTweetRequestAt(Date date);

//    /**
//     * Returns list of message words assigned by user for message filtering
//     * @return words
//     */
//    Set<? extends MessageWord> getWords();
}
