package com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.twitterTweet;

import java.util.ArrayList;

/**
 * List of client tweets
 *
 * <p>
 *      This class implements holder for tweets assigned to client.
 *      Uses usually by web client
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 13.02.2015
 */
public class NotUpToDateTweetsData extends AbstractTweetsListData {
    public NotUpToDateTweetsData() {
        this.isUpToDate = false;
        this.tweets = new ArrayList<>();
    }
}
