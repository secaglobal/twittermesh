package com.secaglobal.twitter.mesh.main.application.services.crypt.impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Implements Default MD5 PasswordEncoder
 *
 * <p> This class realize default password encoder using MD5 algorithm and salt</p>
 *
 * TODO UnitTest
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class SimplePasswordEncoderService implements PasswordEncoder {
    private String salt;

    public SimplePasswordEncoderService(String salt) {
        this.salt = salt;
    }

    /**
     * Encode the raw password.
     * Generally, a good encoding algorithm applies a SHA-1 or greater hash combined with an 8-byte or greater randomly
     * generated salt.
     */
    public String encode(CharSequence rawPassword) {
        return DigestUtils.md5Hex(salt + rawPassword + salt);
    }

    /**
     * Verify the encoded password obtained from storage matches the submitted raw password after it too is encoded.
     * Returns true if the passwords match, false if they do not.
     * The stored password itself is never decoded.
     *
     * @param rawPassword the raw password to encode and match
     * @param encodedPassword the encoded password from storage to compare with
     * @return true if the raw password, after encoding, matches the encoded password from storage
     */
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return encode(rawPassword).equals(encodedPassword);
    }
}
