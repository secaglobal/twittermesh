package com.secaglobal.twitter.mesh.main.application.repositories;

import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;

/**
 * Interface of repository for interaction with message words
 *
 * <p>This interface describes interface for interaction with MessageWord instances</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface MessageWordRepository extends Repository<MessageWord, Long> {

    /**
     * Returns message words assigned to user
     * @params userId user id (identifier)
     * @return user instance
     */
    Iterable<MessageWord> findAllByUserId(Long userId);
}