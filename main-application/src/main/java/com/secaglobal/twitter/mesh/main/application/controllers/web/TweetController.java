package com.secaglobal.twitter.mesh.main.application.controllers.web;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.DefaultResponse;
import com.secaglobal.twitter.mesh.main.application.controllers.internal.AgentController;
import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.twitterTweet.AbstractTweetsListData;
import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.twitterTweet.NotUpToDateTweetsData;
import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.twitterTweet.TweetsListData;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.services.twitter.TweetService;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * Handles Agent specific requests
 *
 * <p>This class provides rest api for requests related to Twitter tweets. In use by public requester<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
@RestController
@RequestMapping("/tweet")
public class TweetController {
    private static final Logger logger = LoggerFactory.getLogger(AgentController.class);

    @Resource(name = "tweetService")
    private TweetService tweetService;

    @Resource(name = "userService")
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public DefaultResponse<AbstractTweetsListData> list() {
        logger.info("Requested client tweets");
        User user = userService.getCurrentUser();

        logger.info("Updating tweets request date for user {}", user.getUsername());
        userService.updateTweetRequestDate(user);

        AbstractTweetsListData data = user.isTweetsUpToDate()
            ? new TweetsListData(tweetService.getTweetRepository().findAllByUserId(user.getId()))
            : new NotUpToDateTweetsData();

        logger.info("Collected requested tweets for user {}", user.getUsername());
        return new DefaultResponse<AbstractTweetsListData>().setData(data);
    }
}
