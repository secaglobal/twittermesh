package com.secaglobal.twitter.mesh.main.application.controllers.web;

import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.user.LoginData;
import com.secaglobal.twitter.mesh.main.application.controllers.web.request.user.LoginForm;
import com.secaglobal.twitter.mesh.main.application.controllers.web.request.user.LoginFormValidator;
import com.secaglobal.twitter.mesh.main.application.controllers.web.request.user.SignupFormValidator;
import com.secaglobal.twitter.mesh.common.messages.Message;
import com.secaglobal.twitter.mesh.common.controllers.responses.impl.DefaultResponse;
import com.secaglobal.twitter.mesh.common.messages.impl.ThrowableMessageHolder;
import com.secaglobal.twitter.mesh.common.messages.impl.SimpleMessage;
import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.user.SignupData;
import com.secaglobal.twitter.mesh.main.application.controllers.web.request.user.SignupForm;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.validation.*;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;


/**
 * Handles user related requests
 *
 * <p>This class provides rest api for requests related to user (aka login, signup, profile)<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
@RestController
@RequestMapping("/user")
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Resource(name = "userService")
    private UserService userService;

    @Autowired
    private ResourceBundleMessageSource messageResolver;

    @InitBinder("loginForm")
    protected void initLoginBinder(WebDataBinder binder) {
        binder.setValidator(new LoginFormValidator());
    }

    @InitBinder("signupForm")
    protected void initSingupBinder(WebDataBinder binder) {
        binder.setValidator(new SignupFormValidator());
    }

    @RequestMapping(value="/signup", method = RequestMethod.POST)
    public DefaultResponse<SignupData> signup(@Valid @RequestBody SignupForm form,
                                             Errors errors,
                                             HttpServletRequest request) throws IOException {

        String username = form.getUsername();
        String password = form.getPassword();

        logger.info("Signup request received: username={} password={}", username, form.getPassword());

        List<Message> messages = new LinkedList<>();

        if (!errors.hasErrors()) {
            try {
                userService.registerUser(username, password);
                userService.loginUser(username, password, new WebAuthenticationDetails(request));
            } catch (ThrowableMessageHolder e) {
                messages.addAll(e.getMessages());
            }
        } else {
            for (ObjectError err : errors.getAllErrors()) {
                messages.add(new SimpleMessage(
                        messageResolver.getMessage(err.getCode(), new Object[]{}, Locale.US)
                ));
            }
        }

        return new DefaultResponse<SignupData>()
                .setData(new SignupData())
                .setErrors(messages);
    }

    @RequestMapping(value="/login", method = RequestMethod.POST)
    public DefaultResponse<LoginData> login(@Valid @RequestBody LoginForm form,
                                              Errors errors,
                                              HttpServletRequest request) throws IOException {

        String username = form.getUsername();
        String password = form.getPassword();

        logger.info("Login request received: username={} password={}", username, form.getPassword());

        List<Message> messages = new LinkedList<>();

        if (!errors.hasErrors()) {
            if (!userService.loginUser(username, password, new WebAuthenticationDetails(request))) {
                String msg = messageResolver.getMessage("common.auth.incorrect_credentials", new Object[]{}, Locale.US);
                messages.add(new SimpleMessage(msg));
            }
        } else {
            for (ObjectError err : errors.getAllErrors()) {
                messages.add(new SimpleMessage(
                        messageResolver.getMessage(err.getCode(), new Object[]{}, Locale.US)
                ));
            }
        }

        return new DefaultResponse<LoginData>()
                .setData(new LoginData())
                .setErrors(messages);
    }
}
