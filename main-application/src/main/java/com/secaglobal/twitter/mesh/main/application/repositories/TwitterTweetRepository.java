package com.secaglobal.twitter.mesh.main.application.repositories;

import com.secaglobal.twitter.mesh.main.application.domains.TwitterTweet;

/**
 * Interface of repository for interaction with message words
 *
 * <p>This interface describes interface for interaction with MessageWord instances</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface TwitterTweetRepository extends Repository<TwitterTweet, Long> {

    /**
     * Returns tweets assigned to user
     * @params userId user id (identifier)
     * @return user instance
     */
    Iterable<TwitterTweet> findAllByUserId(Long userId);

    /**
     * Removes tweets assigned to user
     * @params userId user id (identifier)
     * @return user instance
     */
    void deleteAllByUserId(Long userId);

    /**
     * Removes tweets assigned to user
     * @params userIds user ids (identifier)
     * @return user instance
     */
    void deleteAllByUserId(Iterable<Long> userIds);
}