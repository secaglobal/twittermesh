package com.secaglobal.twitter.mesh.main.application.services.twitter;

import org.springframework.social.oauth1.OAuthToken;

/**
 * Interface provides api for interaction with Twitter OAuth1 authentication
 *
 * <p>This interface describes api for OAuth authentication provided by Twitter</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface TwitterOAuth1Service {

    /**
     * Prepares representation of request token.
     * Uses for authorization and access token verification
     * @param callbackUrl url that receives and processes Twitter access token
     * @return
     * @see TwitterOAuth1Service#getAuthorizeUrl
     * @see TwitterOAuth1Service#varifyAuthorization
     */
    OAuthToken createRequestToken(String callbackUrl);

    /**
     * Prepares Twitter authorization url
     * @return url
     */
    String getAuthorizeUrl(OAuthToken requestToken);

    /**
     * Varify received twitter access token
     */
    OAuthToken varifyAuthorization(OAuthToken requestToken, String oauthVerifier);
}
