package com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.system;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;

/**
 * System environment
 *
 * <p>
 *      This class implements holder for system environment.
 *      Uses usually by web client
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 30.01.2015
 */
public class EnvData extends AbstractData {
    public String username;
    public Boolean authenticated;
    public Boolean twitterAccessGranted;
}
