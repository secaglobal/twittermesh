package com.secaglobal.twitter.mesh.main.application.services.twitter.impl;

import com.secaglobal.twitter.mesh.main.application.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.main.application.repositories.TwitterTweetRepository;
import com.secaglobal.twitter.mesh.main.application.services.twitter.TweetService;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Service provides api for interaction with User domain logic
 *
 * <p>This class provides api for User domain logic</p>
 *
 * TODO UnitTest
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 25.01.2015
 */
public class DefaultTweetService implements TweetService {
    private static final Logger logger = LoggerFactory.getLogger(DefaultTweetService.class);

    private TwitterTweetRepository tweetRepository;
    private UserService userService;

    public DefaultTweetService(TwitterTweetRepository tweetRepository, UserService userService) {
        this.tweetRepository = tweetRepository;
        this.userService = userService;
    }

    /**
     * Returns tweets tweetRepository api
     * @return tweetRepository
     */
    public TwitterTweetRepository getTweetRepository() {
        return tweetRepository;
    }

    /**
     * Saves array of tweets.
     * <p>
     *     This method saves tweets, that recieved buy Agent as new client tweets.
     *     It is not required that all tweets will belongs to one client
     * </p>
     * @param tweets array of tweets
     *
     * TODO Implement bulk saving
     **/
    public void saveNewCollectedTweets(Iterable<? extends TwitterTweet> tweets) {
        logger.info("Refreshing tweets");

        Set<Long> userIds = new LinkedHashSet<>();

        for (TwitterTweet tweet: tweets) {
            userIds.add(tweet.getUserId());
        }

        logger.debug("Deleting old tweets");
        tweetRepository.deleteAllByUserId(userIds);

        if (logger.isTraceEnabled()) {
            for (TwitterTweet tweet: tweets) {
                logger.trace("Saving tweet id = {}, message = {}", tweet.getId(), tweet.getMessage().getBytes());
            }
        }

        logger.debug("Persisting new tweets");
        tweetRepository.save(tweets);

        logger.debug("Refreshing tweets up to date flag");
        userService.setTweetsUpToDateFlagById(userIds);
    }
}
