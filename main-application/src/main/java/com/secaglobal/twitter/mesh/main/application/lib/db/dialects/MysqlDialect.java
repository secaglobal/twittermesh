package com.secaglobal.twitter.mesh.main.application.lib.db.dialects;

import org.hibernate.dialect.MySQL5InnoDBDialect;

public class MysqlDialect extends MySQL5InnoDBDialect {
    public String getTableTypeString() {
        return " ENGINE=InnoDB DEFAULT CHARSET=utf8mb4";
    }
}
