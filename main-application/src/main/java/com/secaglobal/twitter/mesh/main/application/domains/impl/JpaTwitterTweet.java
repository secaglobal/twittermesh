package com.secaglobal.twitter.mesh.main.application.domains.impl;

import com.secaglobal.twitter.mesh.main.application.domains.TwitterTweet;

import javax.persistence.*;
import java.util.Date;

/**
 * Twitter tweet representation
 *
 * <p>
 *     This class represents twitter tweet entity class
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 12.02.2015
 */
@Entity
@Table(name = "TwitterTweet")
public class JpaTwitterTweet implements TwitterTweet {
    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false)
    private Long vendorId;

    @Column(nullable = false, length = 255)
    private String message;

    @Column(nullable = false, length = 255)
    private String authorName;

    @Column()
    @Temporal(TemporalType.TIMESTAMP)
    private Date postedDate;

    @Column(nullable = false)
    private Long userId;

    /**
     * Returns twitter unique identifier
     * @return tweet unique identifier
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets twitter unique identifier
     * @param id twitter unique identifier
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns vendor unique identifier
     * @return tweet unique identifier
     */
    public Long getVendorId() {
        return vendorId;
    }

    /**
     * Sets vendor unique identifier
     * @param id twitter unique identifier
     */
    public void setVendorId(Long id) {
        this.vendorId = id;
    }

    /**
     * Returns tweet message body
     * @return message body
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets tweet message body
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Returns tweet author name
     * @return author name
     */
    public String getAuthor() {
        return this.authorName;
    }

    /**
     * Sets tweet author name
     * @param authorName name of tweet author
     */
    public void setAuthor(String authorName) {
        this.authorName = authorName;
    }

    /**
     * Returns the date when tweet was posted
     * @return the date when tweet was posted
     */
    public Date getPostedDate() {
        return postedDate;
    }

    /**
     * Sets the date when tweet was posted
     * @param date the date when tweet was posted
     */
    public void setPostedDate(Date date) {
        this.postedDate = date;
    }

    /**
     * Returns user id that word is assigned to
     * @return user id (owner id)
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * Set user id that word is assigned to
     * @param userId user id (owner id)
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
