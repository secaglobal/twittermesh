package com.secaglobal.twitter.mesh.main.application.repositories;

import com.secaglobal.twitter.mesh.main.application.domains.User;

/**
 * Interface of repository for interaction of users
 *
 * <p>This interface describes interface for interaction with User instances</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public interface UserRepository extends Repository<User, Long> {

    /**
     * Search user by username
     * @return user instance
     */
    User findByUsername(String username);

    /**
     * Returns all instances that can receive messages
     * @return list of instances
     */
    Iterable<User> findAllWaitingTweetsUpdate();

    /**
     * Sets flag that tweets is up to date and can be used by user
     * @param userIds users ids
     */
    void setTweetsUpToDateFlagById(Iterable<Long> userIds);
}