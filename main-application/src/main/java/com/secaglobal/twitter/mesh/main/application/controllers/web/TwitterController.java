package com.secaglobal.twitter.mesh.main.application.controllers.web;

import com.secaglobal.twitter.mesh.main.application.controllers.web.request.twitter.AuthCallbackForm;
import com.secaglobal.twitter.mesh.main.application.controllers.web.request.twitter.AuthRequestForm;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.services.twitter.TwitterOAuth1Service;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Handles twitter related requests
 *
 * <p>This class provides rest api for requests related to Twitter (like authentication)<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
@RestController
@RequestMapping("/twitter")
public class TwitterController {
    private static final Logger logger = LoggerFactory.getLogger(TwitterController.class);

    @Autowired
    private TwitterOAuth1Service twitterService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/auth-request", method = RequestMethod.GET)
    public String authRequest(@ModelAttribute AuthRequestForm form,
                              HttpSession session,
                              HttpServletRequest request,
                              HttpServletResponse response) throws IOException {

        logger.debug("Twitter authorization request called");

        String clientRedirectUrlEncoded = URLEncoder.encode(form.getRedirectTo(), "UTF-8");
        String callbackPath = request.getContextPath() + "/twitter/auth-callback";
        String callbackQuery = "?redirectTo=" + clientRedirectUrlEncoded;

        String callbackUrl = new URL(request.getScheme(),
                                     request.getServerName(),
                                     request.getServerPort(),
                                     callbackPath + callbackQuery).toString();

        OAuthToken token = twitterService.createRequestToken(callbackUrl);
        String url = twitterService.getAuthorizeUrl(token);

        session.setAttribute("openTwitterAccessToken", token);

        logger.debug("Token {} created. Processing redirect to {}", token.getValue(), url);

        response.sendRedirect(url);

        return "redirect:" + url;
    }

    @RequestMapping(value = "/auth-callback", method = RequestMethod.GET)
    public String authCallback(@ModelAttribute AuthCallbackForm form,
                               HttpSession session,
                               HttpServletRequest request,
                               HttpServletResponse response) throws IOException {

        logger.debug("Twitter authorization callback called");

        OAuthToken requestToken = (OAuthToken) session.getAttribute("openTwitterAccessToken");
        OAuthToken accessToken = twitterService.varifyAuthorization(requestToken, form.getOauth_verifier());

        User user = userService.getRepository().findByUsername(request.getRemoteUser());

        userService.setTwitterAccessToken(user, accessToken);

        session.removeAttribute("openTwitterAccessToken");

        logger.trace("Access Token created.", accessToken.getValue());

        logger.debug("Redirect to: ", form.getRedirectTo());

        response.sendRedirect(form.getRedirectTo());

        return "redirect:" + form.getRedirectTo();
    }
}
