package com.secaglobal.twitter.mesh.main.application.controllers.web;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.DefaultResponse;
import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.system.EnvData;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/**
 * Handles user related requests
 *
 * <p>This class provides rest api for requests related to user (aka login, signup, profile)<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
@RestController
@RequestMapping("/system")
public class SystemController {
    private static final Logger logger = LoggerFactory.getLogger(SystemController.class);

    @Resource(name = "userService")
    UserService userService;

    /**
     * Provides list of properties specific for current user environment,
     * like authorization status or if setup is completed.
     * @param request request
     * @return response
     * @throws IOException
     */
    @RequestMapping(value="/env", method = RequestMethod.GET)
    public DefaultResponse<EnvData> env(HttpServletRequest request) throws IOException {

        logger.debug("System env request received");

        EnvData env = new EnvData();

        env.username = request.getRemoteUser();
        env.authenticated = env.username != null;
        env.twitterAccessGranted = false;

        if (env.authenticated) {
            User user = userService.getRepository().findByUsername(env.username);

            if (user != null) {
                env.twitterAccessGranted = userService.isTwitterSetupCompleted(user);
            }
        }

        logger.debug("Send env {}", env);

        return new DefaultResponse<EnvData>().setData(env);
    }
}
