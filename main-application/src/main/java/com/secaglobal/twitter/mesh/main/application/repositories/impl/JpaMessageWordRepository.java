package com.secaglobal.twitter.mesh.main.application.repositories.impl;

import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.impl.JpaMessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.impl.JpaUser;
import com.secaglobal.twitter.mesh.main.application.repositories.MessageWordRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Repository for interaction of users message words
 *
 * <p>This class describes interface for interaction with MessageWord instances using JPA approach</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class JpaMessageWordRepository extends AbstractJpaRepository<MessageWord> implements MessageWordRepository {

    /**
     * Constructor
     */
    public JpaMessageWordRepository() {
        super(JpaMessageWord.class);
    }

    /**
     * Returns message words assigned to user
     * @params userId user id (identifier
     * @return user instance
     */
    @Transactional
    @SuppressWarnings("unchecked")
    public List<MessageWord> findAllByUserId(Long userId) {
        List words =  entityManager
                .createQuery("select u from JpaMessageWord u where u.userId = :userId")
                .setParameter("userId", userId)
                .getResultList();

        return words;
    }
}