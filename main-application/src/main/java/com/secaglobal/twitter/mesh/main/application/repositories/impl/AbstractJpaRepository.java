package com.secaglobal.twitter.mesh.main.application.repositories.impl;

import com.secaglobal.twitter.mesh.main.application.domains.Entity;
import com.secaglobal.twitter.mesh.main.application.repositories.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.LinkedList;

/**
 * Abstract Repository for interaction with persistent data
 *
 * <p>This class defines common api for interaction with data instances using JPA approach</p>
 *
 * TODO UntTest
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
abstract public class AbstractJpaRepository<T extends Entity> implements Repository<T, Long> {
    private final Logger logger = LoggerFactory.getLogger(AbstractJpaRepository.class);
    private Class<? extends T> type;

    @PersistenceContext
    public EntityManager entityManager;

    /**
     * Constructor
     * @param type type of instance interact with
     */
    public AbstractJpaRepository(Class<? extends T> type) {
        this.type = type;
    }

    /**
     * Creates new message word instance. But do not persist it.
     * @return new instance
     */
    public T create() {
        try {
            return type.newInstance();
        } catch(InstantiationException|IllegalAccessException e) {
            logger.error("Cannot instantiate instance: class = {} exception = {}", type.getName(), e);
            return null;
        }
    }

    /**
     * Persist instances
     * @param instances instances
     */
    @Transactional
    public <S extends T> Iterable<S> save(Iterable<S> instances) {
        for(S instance: instances) {
            save(instance);
        }

        return instances;
    }

    /**
     * Persist instance
     * @param instance instance of data class
     * @return same instance
     */
    @Transactional
    public <S extends T> S save(S instance) {
        entityManager.merge(instance);
        return instance;
    }

    /**
     * Load instance by id
     * @param id id
     * @return same instance
     */
    @Transactional
    @SuppressWarnings("unchecked")
    public T findById(Long id) {
        return (T) entityManager.find(type, id);
    }

    /**
     * Returns list of all available instances
     * @return list of instances
     */
    @Transactional
    @SuppressWarnings("unchecked")
    public java.lang.Iterable<T> findAll() {
        return entityManager.createQuery("select i from  " + type.getName() + " i").getResultList();
    }

    /**
     * Delete instance
     * @param instance instance
     */
    @Transactional
    public void delete(T instance) {
        entityManager.remove(instance);
    }

    /**
     * Delete list of instances
     * @param instances List of users
     */
    @Transactional
    public void delete(Iterable<? extends T> instances) {
        LinkedList<Long> ids = new LinkedList<>();

        for (T instance : instances) {
            ids.add(instance.getId());
        }

        if (ids.isEmpty()) return;

        entityManager.createQuery("delete from " + type.getName() + " i where i.id in (:ids)")
                .setParameter("ids", ids)
                .executeUpdate();
    }
}