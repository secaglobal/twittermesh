package com.secaglobal.twitter.mesh.main.application.domains.impl;

import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.persistence.*;
import java.util.*;

/**
 * Entity User
 */
@Entity
@Table(name = "Client")
public class JpaUser implements User {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false, length = 32)
    private String password;

    @Column(length = 255)
    private String twitterAccessToken;

    @Column(length = 255)
    private String twitterAccessTokenSecret;

    @Column()
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

//    @OneToMany(mappedBy = "user")
//    private Set<JpaMessageWord> words;

    @Column(nullable = false)
    private Boolean tweetsUpToDate = false;

    @Column()
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastTweetRequestAt;

    /**
     * Returns user id (unique)
     * @return identifuer
     */
    public Long getId() {
        return id;
    }

    /**
     * Set user id
     * @param id unique identifier
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns username (unique)
     * @return username
     */

    public String getUsername() {
        return username;
    }

    /**
     * Set user id
     * @param username unique identifier
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns password (hashed)
     * @return hashed password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Password (hashed)
     * @param password hashed password
     */
    public void setPassword(String password) {
        this.password = password;
    }
//
//    /**
//     * Returns list of message words assigned by user for twitter message filtering
//     * @return words
//     */
//    public Set<? extends MessageWord> getWords() {
//        return words;
//    }
//
//    /**
//     * Sets list of message words assigned by user for twitter message filtering
//     * @param words words
//     */
//    public void setWords(Set<JpaMessageWord> words) {
//        this.words = words;
//    }

    /**
     * Returns twitter
     * @return access token
     */
    public String getTwitterAccessToken() {
        return twitterAccessToken;
    }

    /**
     * Sets Twitter token
     * @param token Valid twitter access token
     */
    public void setTwitterAccessToken(String token) {
        this.twitterAccessToken = token;
    }

    /**
     * Returns twitter
     * @return token
     */
    public String getTwitterAccessTokenSecret() {
        return this.twitterAccessTokenSecret;
    }

    /**
     * Sets Twitter token secret
     * @param tokenSecret Valid twitter access token secret
     */
    public void setTwitterAccessTokenSecret(String tokenSecret) {
        this.twitterAccessTokenSecret = tokenSecret;
    }

    /**
     * Returns registration date
     * @return date
     */
    public Date getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets registration date
     * @param date date
     */
    public void setCreatedAt(Date date) {
        this.createdAt = date;
    }


    /**
     * Figure out if current tweets is up to date
     * @return state
     */
    public Boolean isTweetsUpToDate() {
        return this.tweetsUpToDate;
    }

    /**
     * Sets if current tweets is up to date
     * @param state current state
     */
    public void setTweetsUpToDate(Boolean state) {
        this.tweetsUpToDate = state;
    }

    /**
     * Returns date when tweets was requested last
     * This date shows how long ago client request tweets.
     * It is necessary for organizing tweets refreshing
     * @return date
     */
    public Date getLastTweetRequestAt() {
        return this.lastTweetRequestAt;
    }

    /**
     * Sets date when tweets was requested last
     * This date shows how long ago client request tweets.
     * It is necessary for organizing tweets refreshing
     * @param date date
     */
    public void setLastTweetRequestAt(Date date) {
        this.lastTweetRequestAt = date;
    }

    /**
     * Returns the authorities granted to the user. Cannot return <code>null</code>.
     *
     * @return the authorities, sorted by natural key (never <code>null</code>)
     */
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<SimpleGrantedAuthority> roles = new  ArrayList<>(1);

        roles.add(new SimpleGrantedAuthority("ROLE_USER"));

        return roles;
    }

    /**
     * Indicates whether the user's account has expired. An expired account cannot be authenticated.
     * @return <code>true</code> if enabled or <code>false</code>
     */
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Indicates whether the user is locked or unlocked. A locked user cannot be authenticated.
     * @return <code>true</code> if enabled or <code>false</code>
     */
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Indicates whether the user's credentials (password) has expired. Expired credentials prevent
     * authentication.
     * @return <code>true</code> if enabled or <code>false</code>
     */
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Indicates whether the user is enabled or disabled. A disabled user cannot be authenticated.
     * @return <code>true</code> if enabled or <code>false</code>
     */
    public boolean isEnabled() {
        return true;
    }
}
