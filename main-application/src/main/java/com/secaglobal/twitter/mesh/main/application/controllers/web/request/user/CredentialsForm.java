package com.secaglobal.twitter.mesh.main.application.controllers.web.request.user;

import org.springframework.validation.annotation.Validated;

/**
 * Describes credentials form
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
@Validated
abstract public class CredentialsForm {
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}


