package com.secaglobal.twitter.mesh.main.application.services.twitter;

import com.secaglobal.twitter.mesh.main.application.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.main.application.repositories.TwitterTweetRepository;

/**
 * Interface provides api for Tweets Service
 *
 * <p>This interface describes api for business logic related to Twitter tweets domain</p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 13.02.2015
 */
public interface TweetService {

    /**
     * Returns tweets repository api
     * @return repository
     */
    TwitterTweetRepository getTweetRepository();

    /**
     * Saves array of tweets.
     * <p>
     *     This method saves tweets, that received from Agent as new client tweets.
     *     It is not required that all tweets will belongs to one client.
     *     This method responsible for cleaning client previous tweets as well.
     * </p>
     * @param tweets array of tweets
     */
    void saveNewCollectedTweets(Iterable<? extends TwitterTweet> tweets);
}
