package com.secaglobal.twitter.mesh.main.application.filters.auth;

import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Check if user complete Twitter Authorization
 *
 * <p>
 *      This class checks if user set up twitter account properly.
 *      If not it provide a way to complete the setup
 * <p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 28.01.2015
 *
 * TODO UnitTest
 */
public class TwitterSetupFilter implements Filter {
    private UserService userService;
    private String path = "/twitter/auth-request";

    /**
     * Constructor
     */
    public TwitterSetupFilter(UserService userService) {
        this.userService = userService;
    }

    /**
     * Initialize request
     * @param filterConfig configuration
     * @throws ServletException
     */
    public void init(FilterConfig filterConfig) throws ServletException {}


    /**
     * Filter out requests of user, that do not complete twitter setup
     * @param request Servlet request
     * @param response Servlet response
     * @param chain Filters chain. Needs to prolong chain traversing
     * @throws IOException
     * @throws ServletException
     */
    public void doFilter ( ServletRequest request, ServletResponse response, FilterChain chain )
            throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String username = req.getRemoteUser();

        if (username != null) {
            User user = userService.getRepository().findByUsername(username);

            if (user != null && !userService.isTwitterSetupCompleted(user)) {
                res.sendRedirect(getRedirectUrl(req));
                return;
            }
        }

        chain.doFilter(request, response);
    }

    /**
     * Filter destructor
     */
    public void destroy() {}

    /**
     * Sets redirect uri. Uri will be used like context path.
     * E.g. if application entrypoint is /main-app/, the required uri will be /main-app/{path}
     * @param path uri
     * @uses HttpServletRequest#getContextPath
     */
    public void setContextUri(String path) {
        this.path = path;
    }

    private String getRedirectUrl(HttpServletRequest request) {
        return request.getContextPath() + path;
    }

}
