package com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.user;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;

/**
 * Signup response data
 *
 * <p>
 *      This class implements holder for signup response data.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class SignupData extends AbstractData {}
