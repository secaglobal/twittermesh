package com.secaglobal.twitter.mesh.main.application.controllers.internal.responses.agent;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;

/**
 * Response on Twitter tweets saving
 *
 * <p>
 *      This class implements holder for answer on saving Twitter tweets assigned to concrete client.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 13.02.2015
 */
public class TweetsSavedData extends AbstractData {}
