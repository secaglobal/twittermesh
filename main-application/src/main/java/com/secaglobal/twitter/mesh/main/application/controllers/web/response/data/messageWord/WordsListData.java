package com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.messageWord;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.AbstractData;

/**
 * List of words defined by user for searching
 *
 * <p>
 *      This class implements holder for message words defined by user for searching.
 *      Uses usually by web client
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 02.02.2015
 */
public class WordsListData extends AbstractData {
    public Iterable<String> words;

    public WordsListData(Iterable<String> words) {
        this.words = words;
    }
}
