package com.secaglobal.twitter.mesh.main.application.repositories.impl;

import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.domains.impl.JpaUser;
import com.secaglobal.twitter.mesh.main.application.repositories.UserRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Calendar;
import java.util.List;

/**
 * Repository for interaction of users
 *
 * <p>This class describes interface for interaction with User instances using JPA approach</p>
 *
 * TODO UntTest
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
public class JpaUserRepository extends AbstractJpaRepository<User> implements UserRepository {

    @PersistenceContext
    public EntityManager entityManager;

    /**
     * Constructor
     */
    public JpaUserRepository() {
        super(JpaUser.class);
    }

    /**
     * Request one instance with specified username
     * @param username instance of class {@link User @User}
     * @return same instance
     */
    @Transactional
    public User findByUsername(String username) {
        List users =  entityManager
                .createQuery("select u from JpaUser u where u.username = :username")
                .setParameter("username", username)
                .setMaxResults(1)
                .getResultList();

        return users.size() > 0 ? (User)users.get(0) : null;
    }

    /**
     * Returns all instances that can receive messages
     * @return list of instances
     */
    @Transactional
    @SuppressWarnings("unchecked")
    public Iterable<User> findAllWaitingTweetsUpdate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MINUTE, -5);

        return entityManager.createQuery(
                "select i from JpaUser i where i.twitterAccessToken is not null and i.lastTweetRequestAt >= :date"
        ).setParameter("date", calendar.getTime()).getResultList();
    }

    /**
     * Sets flag that tweets is up to date and can be used by user
     * @param userIds users ids
     */
    @Transactional
    public void setTweetsUpToDateFlagById(Iterable<Long> userIds) {
        entityManager
            .createQuery("update JpaUser u set u.tweetsUpToDate = true where u.id in (:userIds)")
            .setParameter("userIds", userIds)
            .executeUpdate();
    }
}