package com.secaglobal.twitter.mesh.main.application.services.user.impl;

import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.domains.validators.UserValidator;
import com.secaglobal.twitter.mesh.common.messages.Message;
import com.secaglobal.twitter.mesh.common.messages.impl.SimpleMessage;
import com.secaglobal.twitter.mesh.common.messages.impl.ThrowableMessageHolder;
import com.secaglobal.twitter.mesh.main.application.repositories.MessageWordRepository;
import com.secaglobal.twitter.mesh.main.application.repositories.TwitterTweetRepository;
import com.secaglobal.twitter.mesh.main.application.repositories.UserRepository;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.validation.*;

import java.util.*;

/**
 * Service provides api for interaction with User domain logic
 *
 * <p>This class provides api for User domain logic</p>
 *
 * TODO UnitTest
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 25.01.2015
 */
public class DefaultUserService implements UserService, UserDetailsService {
    private static final Logger logger = LoggerFactory.getLogger(DefaultUserService.class);

    private UserRepository repository;
    private UserValidator userValidator;
    private ResourceBundleMessageSource messageResolver;
    private PasswordEncoder passwordEncoder;
    private MessageWordRepository wordRepository;
    private TwitterTweetRepository tweetRepository;

    public DefaultUserService(
            UserRepository repository,
            UserValidator userValidator,
            ResourceBundleMessageSource messageResolver,
            PasswordEncoder passwordEncoder,
            MessageWordRepository wordRepository,
            TwitterTweetRepository tweetRepository) {

        this.repository = repository;
        this.userValidator = userValidator;
        this.messageResolver = messageResolver;
        this.passwordEncoder = passwordEncoder;
        this.wordRepository = wordRepository;
        this.tweetRepository = tweetRepository;
    }

    /**
     * Register new user
     * <p>Register new user in system. Provide validation and checks if user already registered</p>
     * @return user
     * @throws ThrowableMessageHolder
     */
    public User registerUser(String username, String password) throws ThrowableMessageHolder {
        logger.info("New user registration: {}", username);

        User user = repository.create();
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setCreatedAt(new Date());

        DataBinder binder = new DataBinder(user);
        binder.addValidators(userValidator);
        binder.validate();
        BindingResult result = binder.getBindingResult();

        if (result.hasErrors()) {
            logger.info("New user registration failed: found errors");
            LinkedList<Message> messages = new LinkedList<>();

            for (ObjectError err : result.getAllErrors()) {
                String msg = messageResolver.getMessage(err.getCode(), new Object[]{}, Locale.US);

                messages.add(new SimpleMessage(msg));

                logger.debug("New user registration error: {}", msg);
            }

            throw new ThrowableMessageHolder(messages);
        }

        return repository.save(user);
    }

    /**
     * Locates the user based on the username. In the actual implementation, the search may possibly be case
     * sensitive, or case insensitive depending on how the implementation instance is configured. In this case, the
     * <code>UserDetails</code> object that comes back may have a username that is of a different case than what was
     * actually requested..
     *
     * @param username the username identifying the user whose data is required.
     *
     * @return a fully populated user record (never <code>null</code>)
     *
     * @throws org.springframework.security.core.userdetails.UsernameNotFoundException if the user could not be found or the user has no GrantedAuthority
     */
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.debug("Loading user by username: " + username);

        User user = repository.findByUsername(username);

        if (user == null) {
            logger.debug("User not found: " + username);

            throw new UsernameNotFoundException("User not found: " + username);
        }

        return user;
    }

    /**
     * Login user
     * @return user
     */
    public boolean loginUser(String username, String password, WebAuthenticationDetails details) {
        logger.debug("Logging user with username: " + username);
        User user = repository.findByUsername(username);

        if (user == null || !passwordEncoder.matches(password, user.getPassword())) return false;

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        token.setDetails(details);
        SecurityContextHolder.getContext().setAuthentication(token);
        return true;
    }

    /**
     * Returns user authorized in current security context
     * @return user
     */
    public User getCurrentUser() {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return repository.findById(user.getId());
    }

    /**
     * Find out if user complete Twitter setup
     * @return user
     */
    public boolean isTwitterSetupCompleted(User user) {
        String token = user.getTwitterAccessToken();
        return token != null && !token.isEmpty();
    }

    /**
     * Sets oauth twitter token for user
     * @param user user
     * @param token Twitter Access token (OAuth1)
     */
    public void setTwitterAccessToken(User user, OAuthToken token) {
        logger.debug("Setting access token for user:  username = {}", user.getUsername());
        logger.trace("Setting access token for user:  username = {} token = {} ", user.getUsername(), token);

        user.setTwitterAccessToken(token.getValue());
        user.setTwitterAccessTokenSecret(token.getSecret());

        repository.save(user);
    }

    /**
     * Sets date when tweets request was done by user.
     * @param user user
     */
    public void updateTweetRequestDate(User user) {
        logger.debug("Updating user last tweets request date by {}", user.getUsername());
        user.setLastTweetRequestAt(new Date());
        repository.save(user);
        logger.debug("Updated user last tweets request date by {}", user.getUsername());
    }

    /**
     * Reset flag that tweets is up to date. So client shouldn't use them
     * @param user user
     */
    public void resetTweetsUpToDateFlag(User user) {
        logger.debug("Reseting flag that tweets is up to date for user {}", user.getUsername());
        user.setTweetsUpToDate(false);
        repository.save(user);
        logger.debug("Reset flag that tweets is up to date for user {}", user.getUsername());
    }

    /**
     * Sets flag that tweets is up to date and can be used by user
     * @param user user
     */
    public void setTweetsUpToDateFlag(User user) {
        logger.debug("Seting flag that tweets is up to date for user {}", user.getUsername());
        user.setTweetsUpToDate(true);
        repository.save(user);
        logger.debug("Set flag that tweets is up to date for user {}", user.getUsername());
    }

    /**
     * Sets flag that tweets is up to date and can be used by user
     * @param userIds users ids
     */
    public void setTweetsUpToDateFlagById(Iterable<Long> userIds) {
        logger.debug("Setting flag that tweets is up to date for users with ids {}", userIds);
        repository.setTweetsUpToDateFlagById(userIds);
        logger.debug("Has been set flag that tweets is up to date for users with ids {}", userIds);
    }

    /**
     * Returns list of words that should be used for message filtering
     * @param user user
     * @return words List of new words
     */
    public Iterable<MessageWord> getMessageWords(User user) {
        return wordRepository.findAllByUserId(user.getId());
    }

    /**
     * Specify witch words should be used for message filtering
     * This method should reset current user tweets.
     * @param user user
     * @param words List of new words
     *
     * TODO Implement bulk saving
     */
    public void assignMessageWords(User user, Set<String> words) {
        logger.debug("Assigning new filter words for user {}", user.getUsername());
        tweetRepository.deleteAllByUserId(user.getId());
        wordRepository.delete(getMessageWords(user));

        for (String word: words) {
            logger.debug("Assigning filter word `{}` to user {}", word, user.getUsername());
            MessageWord messageWord = wordRepository.create();
            messageWord.setUserId(user.getId());
            messageWord.setWord(word);

            wordRepository.save(messageWord);
        }

        logger.debug("Assigned new filter words for user {}", user.getUsername());
    }

    /**
     * Returns list of users, that should be processed next by Agent
     * @param limit
     * @return list of users
     */
    public Iterable<User> getUsersToMessageUpdate(int limit) {
        return repository.findAll();
    }

    /**
     * Returns user repository api
     * @return repository
     */
    public UserRepository getRepository() {
        return repository;
    }
}
