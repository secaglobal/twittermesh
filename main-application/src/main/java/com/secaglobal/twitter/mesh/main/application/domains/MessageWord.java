package com.secaglobal.twitter.mesh.main.application.domains;

/**
 * MessageWord Domain Interface
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
public interface MessageWord extends Entity {

    /**
     * Returns word
     * @return word
     */
    String getWord();

    /**
     * Set word
     * @param word word
     */
    void setWord(String word);

    /**
     * Returns user id that word is assigned to
     * @return user id (owner id)
     */
    Long getUserId();

    /**
     * Set user id that word is assigned to
     * @param userId user id (owner id)
     */
    void setUserId(Long userId);
}
