package com.secaglobal.twitter.mesh.main.application.controllers.web.request.user;

import com.secaglobal.twitter.mesh.main.application.utils.ValidationUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Provides validation of credentials form
 *
 * <p>This class provides methods for validation login form received from web client<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
abstract public class CredentialsFormValidator implements Validator {

    /**
     * Checks if supplied class allowed for validation
     * @param clazz Class
     * @return status
     */
    public boolean supports(Class<?> clazz) {
        return CredentialsForm.class.isAssignableFrom(clazz);
    }

    /**
     * Validate target. Place errors into Errors collection
     *
     * Todo UnitTest
     */
    public void validate(Object target, Errors errors) {
        CredentialsForm form = (CredentialsForm) target;


        validatePassword(form, errors);
        validateUsername(form, errors);
    }

    private void validatePassword(CredentialsForm form, Errors errors) {
        String password = form.getPassword();

        if (password == null || ValidationUtils.isLess(password.length(), 6)) {
            errors.rejectValue("password", "common.password.too_short");
        } else if (ValidationUtils.isGreater(password.length(), 255)) {
            errors.rejectValue("password", "common.password.too_long");
        }
    }

    private void validateUsername(CredentialsForm form, Errors errors) {
        String username = form.getUsername();

        if (username == null || !ValidationUtils.isValidEmail(username)) {
            errors.rejectValue("email", "common.email.invalid_email");
        }
    }
}
