package com.secaglobal.twitter.mesh.main.application.domains;

import java.io.Serializable;

/**
 * Entity Domain Interface
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 02.02.2015
 */
public interface Entity extends Serializable {

    /**
     * Returns user id (unique)
     * @return identifuer
     */
    Long getId();

    /**
     * Set user id
     * @param id unique identifier
     */
    void setId(Long id);
}
