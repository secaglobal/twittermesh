package com.secaglobal.twitter.mesh.main.application.controllers.web;

import com.secaglobal.twitter.mesh.common.controllers.responses.impl.DefaultResponse;
import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.messageWord.WordsListData;
import com.secaglobal.twitter.mesh.main.application.controllers.web.response.data.messageWord.WordsSavedData;
import com.secaglobal.twitter.mesh.main.application.controllers.web.request.messageWord.SaveWordsForm;
import com.secaglobal.twitter.mesh.main.application.domains.MessageWord;
import com.secaglobal.twitter.mesh.main.application.domains.User;
import com.secaglobal.twitter.mesh.main.application.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Handles message word related requests
 *
 * <p>This class provides rest api for requests related to message words (aka list, get, save)<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 23.01.2015
 */
@RestController
@RequestMapping("/message-word")
public class MessageWordController {
    private static final Logger logger = LoggerFactory.getLogger(MessageWordController.class);

    @Resource(name = "userService")
    UserService userService;

    /**
     * Prepare list of words that uses for tweets filtering
     * Do not provided checking if user is authorized
     * @return response
     */
    @RequestMapping(method = RequestMethod.GET)
    public DefaultResponse<WordsListData> list() {
        User user = userService.getCurrentUser();

        logger.info("Requested words list for user: {}", user.getUsername());

        Iterable<? extends MessageWord> words = userService.getMessageWords(user);
        List<String> wordsList = new LinkedList<>();

        for (MessageWord word: words) wordsList.add(word.getWord());

        return new DefaultResponse<WordsListData>().setData(new WordsListData(wordsList));
    }

    /**
     * Prepare list of words that uses for tweets filtering
     * Do not provided checking if user is authorized
     * @return response
     */
    @RequestMapping(method = RequestMethod.POST)
    public DefaultResponse<WordsSavedData> save(@RequestBody SaveWordsForm form) {
        User user = userService.getCurrentUser();
        Set<String> words = form.getWords();

        logger.info("Requested words saving {} for {}", words, user.getUsername());

        logger.info("Assigning new filter words for user {}", user.getUsername());
        userService.assignMessageWords(user, words);

        logger.info("Reset tweets `up to data` flag for user {}", user.getUsername());
        userService.resetTweetsUpToDateFlag(user);

        logger.info("Assigned new filter words for user {}", user.getUsername());

        return new DefaultResponse<WordsSavedData>().setData(new WordsSavedData());
    }
}
