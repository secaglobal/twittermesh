package com.secaglobal.twitter.mesh.main.application.controllers.web.request.user;

/**
 * Provides validation of login form
 *
 * <p>This class provides methods for validation login form received from web client<p/>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 01.02.2015
 */
public class LoginFormValidator extends CredentialsFormValidator {

    /**
     * Checks if supplied class allowed for validation
     * @param clazz Class
     * @return status
     */
    public boolean supports(Class<?> clazz) {
        return LoginForm.class.isAssignableFrom(clazz);
    }
}
