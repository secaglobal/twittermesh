package com.secaglobal.twitter.mesh.agent.lib.job.impl;

import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutorBuilder;
import com.secaglobal.twitter.mesh.agent.services.mainApplication.MainApplicationService;
import com.secaglobal.twitter.mesh.agent.services.twitter.TwitterService;

/**
 * Run job logic
 *
 * <p>
 *     This class implements api for interaction with Agent jobs.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 08.02.2015
 */
public class PullUserTweetsJobExecutorBuilder implements JobExecutorBuilder<PullUserTweetsJobExecutor> {
    /**
     * Twitter service
     */
    private TwitterService twitterService;

    /**
     * Twitter service
     */
    private MainApplicationService mainApplicationService;

    public PullUserTweetsJobExecutorBuilder(TwitterService twitterService,
                                            MainApplicationService mainApplicationService) {

        this.twitterService = twitterService;
        this.mainApplicationService = mainApplicationService;
    }

    /**
     * Build executor
     */
    public PullUserTweetsJobExecutor build() {
        PullUserTweetsJobExecutor executor = new PullUserTweetsJobExecutor();
        executor.setMainApplicationService(mainApplicationService);
        executor.setTwitterService(twitterService);
        return executor;
    }
}
