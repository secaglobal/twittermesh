package com.secaglobal.twitter.mesh.agent.schedules;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Tweets Proxy
 *
 * <p>
 *     Gets information (tweets, etc) from Twitter and send it to client side
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public class ConfigUpdater implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(MainAppJobsSchedule.class);

    public void run() {
        logger.info("Task started: {}", ConfigUpdater.class.getSimpleName());

        logger.info("DO NOTHING NOW. NEEDS IMPLEMENTATION");

        logger.info("Task completed: {}", ConfigUpdater.class.getSimpleName());
    }
}

