package com.secaglobal.twitter.mesh.agent.repositories;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;

/**
 * Main Application Server Repository interface
 *
 * <p>
 *     This class provides interface for client server tweets repository.
 *     Client server is a server responsible for web tweets representation
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public interface MainApplicationTwitterTweetRepository {

    /**
     * Save tweets
     * @param tweets List of tweets
     *
     * TODO UnitTest
     */
    void save(Iterable<TwitterTweet> tweets);
}
