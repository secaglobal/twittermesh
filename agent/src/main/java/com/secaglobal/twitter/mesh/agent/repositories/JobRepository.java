package com.secaglobal.twitter.mesh.agent.repositories;

import com.secaglobal.twitter.mesh.common.domains.Job;

/**
 * Job Repository interface
 *
 * <p>
 *     This class describes interface for getting jobs that should be processed by Aggent
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 06.02.2015
 */
public interface JobRepository {

    /**
     * Returns list of jobs that should be executed by agent
     * @return list of jobs
     */
    java.lang.Iterable<Job> findAllPending();
}
