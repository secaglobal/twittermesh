package com.secaglobal.twitter.mesh.agent.lib.job.impl;

import com.secaglobal.twitter.mesh.common.domains.Job;
import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutor;
import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutorFactory;

import java.util.Map;

/**
 * Define which executor should handle a job data
 *
 * <p>
 *     This class implements api for getting Agent job executor by job data class.
 *     Map responsible for assigning executor with data must be passed through
 *     {@link DefaultJobExecutorFactory#setExecutors @setExecutors}
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 08.02.2015
 */
public class DefaultJobExecutorFactory implements JobExecutorFactory {
    Map<Class<? extends Job>, Class<? extends JobExecutor>> pool;

    /**
     * Sets a pool of executors.
     * <p>
     *    Sets a pool of executors. A pool is a map that assign job data class to a executor class.
     *    This pool uses for retrieving information about executor
     * </p>
     * @param pool map
     */
    public void setExecutors(Map<Class<? extends Job>, Class<? extends JobExecutor>> pool) {
        this.pool = pool;
    }

    /**
     * Find out Agent job executor by job data class
     * @param clazz job data class
     * @return job executor
     */
    public Class<? extends JobExecutor> getExecutor(Class<? extends Job> clazz) {
        return pool.get(clazz);
    }
}
