package com.secaglobal.twitter.mesh.agent.lib.job;

import com.secaglobal.twitter.mesh.common.domains.Job;

/**
 * Run job logic
 *
 * <p>
 *     This interface describe api for interaction with Agent jobs.
 *     Executor should be implemented as JavaBean. And have default constructor.
 *     Executor cannot be run if no data was assigned.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 08.02.2015
 */
public interface JobExecutor<T extends Job> extends Runnable {

    /**
     * Assign data to the executor. Executor cannot be run if have no initial data.
     * @param data data (Job data)
     */
    void setData(T data);
}
