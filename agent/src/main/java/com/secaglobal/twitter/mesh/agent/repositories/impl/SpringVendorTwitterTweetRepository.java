package com.secaglobal.twitter.mesh.agent.repositories.impl;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.agent.repositories.VendorTwitterTweetRepository;
import com.secaglobal.twitter.mesh.agent.utils.TweetUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Default Official Twitter tweet repository
 *
 * <p>
 *     This class provides interface for official twitter tweets repository.
 *     As official repository means official twiiter api.
 *     This is a default implementation.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 *
 * TODO UnitTest
 */
public class SpringVendorTwitterTweetRepository implements VendorTwitterTweetRepository {
    private final Logger logger = LoggerFactory.getLogger(SpringVendorTwitterTweetRepository.class);

    /**
     * Retrieve tweets
     * @param connection connection to Twiiter
     * @param filter list of words for filtering
     * @param limit limit of tweets. method will return last specified number of tweets
     * @return tweets
     */
    public Collection<TwitterTweet> findAll(Connection<Twitter> connection,
                                            Set<String> filter,
                                            int limit) {
        logger.debug("Requested tweets from Twitter search api");

        String filterStr = StringUtils.collectionToDelimitedString(filter, " ");

        List<Tweet> vendorTweets =
                connection.getApi().searchOperations().search(filterStr, limit).getTweets();

        List<TwitterTweet> tweets = new ArrayList<>(vendorTweets.size());

        for(Tweet t: vendorTweets) {
            tweets.add(TweetUtils.convertToTweet(t));
        }

        return tweets;
    }
}
