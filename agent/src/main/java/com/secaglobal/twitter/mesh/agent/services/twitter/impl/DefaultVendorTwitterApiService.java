package com.secaglobal.twitter.mesh.agent.services.twitter.impl;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.agent.repositories.VendorTwitterTweetRepository;
import com.secaglobal.twitter.mesh.agent.services.twitter.VendorTwitterApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.connect.Connection;
import org.springframework.social.oauth1.OAuthToken;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.connect.TwitterConnectionFactory;

import java.util.Collection;
import java.util.Set;

/**
 * Official Twitter Api Service Implementation
 *
 * <p>
 *     This service class provide interface that organise interaction with official twitter api
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 * @see com.secaglobal.twitter.mesh.agent.services.twitter.VendorTwitterApiService
 */
public class DefaultVendorTwitterApiService implements VendorTwitterApiService {
    private final Logger logger = LoggerFactory.getLogger(DefaultVendorTwitterApiService.class);
    private VendorTwitterTweetRepository tweetRepository;
    private TwitterConnectionFactory twitterConnectionFactory;

    /**
     * Constructor
     * @param tweetRepository repository for tweets
     */
    public DefaultVendorTwitterApiService(VendorTwitterTweetRepository tweetRepository,
                                          TwitterConnectionFactory twitterConnectionFactory) {

        this.tweetRepository = tweetRepository;
        this.twitterConnectionFactory = twitterConnectionFactory;
    }

    /**
     * Returns tweets
     * @param token Twitter oauth token for creating connection
     * @param filter list of words for filtering
     * @return tweets
     */
    public Collection<TwitterTweet> getTweets(OAuthToken token, Set<String> filter) {
        logger.debug("Extracting tweets from Twitter Repository");

        return tweetRepository.findAll(createConnection(token), filter, 100);
    }

    /**
     * Creates connection to witter
     * @param token oauth1 credentials
     * @return connection to Twitter
     */
    private Connection<Twitter> createConnection(OAuthToken token) {
        logger.debug("Creating Twitter connection");
        logger.trace("Creating Twitter connection {}", token);
        return twitterConnectionFactory.createConnection(token);
    }
}
