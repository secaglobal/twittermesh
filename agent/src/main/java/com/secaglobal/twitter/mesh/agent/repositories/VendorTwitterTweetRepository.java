package com.secaglobal.twitter.mesh.agent.repositories;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import org.springframework.social.connect.Connection;
import org.springframework.social.twitter.api.Twitter;

import java.util.Collection;
import java.util.Set;

/**
 * Official Twitter tweet repository interface
 *
 * <p>
 *     This class provides interface for official twitter tweets repository.
 *     As official repository means official twitter api.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public interface VendorTwitterTweetRepository {

    /**
     * Retrieve tweets
     * @param connection connection to twiiter
     * @param filter list of words for filtering
     * @param limit limit of tweets. method will return last specified number of tweets
     * @return tweets
     */
    Collection<TwitterTweet> findAll(Connection<Twitter> connection, Set<String> filter, int limit);
}
