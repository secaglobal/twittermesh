package com.secaglobal.twitter.mesh.agent.lib.job.impl;

import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutor;
import com.secaglobal.twitter.mesh.agent.services.mainApplication.MainApplicationService;
import com.secaglobal.twitter.mesh.agent.services.twitter.TwitterService;
import com.secaglobal.twitter.mesh.common.domains.PullUserTweetsJob;
import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.social.oauth1.OAuthToken;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Run job logic
 *
 * <p>
 *     This class implements api for interaction with Agent jobs.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 08.02.2015
 */
public class PullUserTweetsJobExecutor implements JobExecutor<PullUserTweetsJob> {
    private final Logger logger = LoggerFactory.getLogger(PullUserTweetsJobExecutor.class);
    private PullUserTweetsJob data;
    private TwitterService twitterService;
    private MainApplicationService mainApplicationService;

    /**
     * Assign data to the executor. Executor cannot be run if have no initial data.
     * @param data data (Job data)
     */
    public void setData(PullUserTweetsJob data) {
        this.data = data;
    }

    /**
     * Sets twitter service
     * @param twitterService twitter service
     */
    public void setTwitterService(TwitterService twitterService) {
        this.twitterService = twitterService;
    }

    /**
     * Sets main application service
     * @param mainApplicationService main application service
     */
    public void setMainApplicationService(MainApplicationService mainApplicationService) {
        this.mainApplicationService = mainApplicationService;
    }

    /**
     * Runs executor
     */
    public void run() {
        logger.info("Job executor started started: {}", PullUserTweetsJobExecutor.class.getSimpleName());


        OAuthToken token = new OAuthToken(data.getTwitterAccessToken(), data.getTwitterAccessTokenSecret());
        Set<String> filter = new HashSet<>();

        for (String word: data.getWords()) filter.add(word);

        Collection<TwitterTweet> tweets = twitterService.getTweets(token, filter);

        for(TwitterTweet tweet: tweets) {
            tweet.setUserId(data.getUserId());
        }

        logger.debug("Extracted tweets count: {}", tweets.size());

        if (tweets.size() > 0) {
            logger.debug("Sending tweets to main application");

            mainApplicationService.sendTweets(tweets);
        } else {
            logger.debug("No tweets extracted. No main application server requests");
        }


        logger.info("Job executor completed: {}", PullUserTweetsJobExecutor.class.getSimpleName());
    }
}
