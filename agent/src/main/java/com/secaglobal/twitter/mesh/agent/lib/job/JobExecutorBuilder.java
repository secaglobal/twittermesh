package com.secaglobal.twitter.mesh.agent.lib.job;

import com.secaglobal.twitter.mesh.common.domains.Job;

/**
 * Build job executor
 *
 * <p>
 *     This interface describe api for creation job executor.
 *     This interface required to support initialization customization
 *     for different executor types.
 *     E.g. some executors will require some services, with builder and builder factory,
 *     it is easy can go with dependency injection.
 *     Presumed that builder will be stateless, so will be thread safe
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 08.02.2015
 */
public interface JobExecutorBuilder<T extends JobExecutor> {

    /**
     * Build executor
     */
    T build();
}
