package com.secaglobal.twitter.mesh.agent.lib.job.impl;

import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutor;
import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutorBuilder;
import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutorBuilderFactory;

import java.util.Map;

/**
 * Define which builde should create a job executor
 *
 * <p>
 *     This class implements api for getting Agent job executor builder by job executor class.
 *     Map responsible for assigning executor builder with executor must be passed through
 *     {@link com.secaglobal.twitter.mesh.agent.lib.job.impl.DefaultJobExecutorBuilderFactory#setBuilders @setBuilders}
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 08.02.2015
 */
public class DefaultJobExecutorBuilderFactory implements JobExecutorBuilderFactory {
    Map<Class<? extends JobExecutor>, JobExecutorBuilder> pool;

    /**
     * Sets a pool of builders.
     * <p>
     *    Sets a pool of executors builders. A pool is a map that assign job executor class to a builder class.
     *    This pool uses for retrieving information about executor's builder
     * </p>
     * @param pool map
     */
    public void setBuilders(Map<Class<? extends JobExecutor>, JobExecutorBuilder> pool) {
        this.pool = pool;
    }


    /**
     * Find out Agent job executor by job data class
     * @param clazz job data class
     * @return job executor
     */
    public JobExecutorBuilder getBuilder(Class<? extends JobExecutor> clazz) {
        return pool.get(clazz);
    }
}
