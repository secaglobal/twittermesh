package com.secaglobal.twitter.mesh.agent.services.twitter;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import org.springframework.social.oauth1.OAuthToken;

import java.util.Collection;
import java.util.Set;

/**
 * Twitter Service Interface
 *
 * <p>
 *     This service class provide interface that organise interaction with twitter provider
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public interface TwitterService {

    /**
     * Returns tweets
     * @param token Twitter oauth token for creating connection
     * @param filter list of words for filtering
     * @return tweets
     */
    public Collection<TwitterTweet> getTweets(OAuthToken token, Set<String> filter);
}
