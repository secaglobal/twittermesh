package com.secaglobal.twitter.mesh.agent.utils;

import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.common.domains.impl.DefaultTwitterTweet;
import org.springframework.social.twitter.api.Tweet;

/**
 * Utils for tweets
 *
 * <p>
 *     This class provides utils for tweets converting, manage, etc.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 18.01.2015
 */
public class TweetUtils {

    /**
     * Converts vendor tweet to local tweet
     * <p>This method converts tweets provided by twitter api to local tweet representation</p>
     * TODO Unit Test
     * @param vendorTweet twiiter tweet
     * @return local tweet representation
     */
    public static TwitterTweet convertToTweet(Tweet vendorTweet) {
        TwitterTweet tweet = new DefaultTwitterTweet();
        tweet.setVendorId(vendorTweet.getId());
        tweet.setMessage(vendorTweet.getText());
        tweet.setAuthor(vendorTweet.getFromUser());
        tweet.setPostedDate(vendorTweet.getCreatedAt());
        return tweet;
    }
}
