package com.secaglobal.twitter.mesh.agent.lib.job;

import com.secaglobal.twitter.mesh.common.domains.Job;

/**
 * Define which executor builder should handle a job executor creation
 *
 * <p>
 *     This interface describe api for detection Agent job executor builder by job executor clas.
 *     The main idea of this factory just provide api for retrieving job executor builder. but
 *     the way of factory initialization, will depends on realization. E.g. some realizations will
 *     consist with map statically filled or filled using constructor in time initialization.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 08.02.2015
 */
public interface JobExecutorBuilderFactory {

    /**
     * Find out Agent job executor by job data class
     * @param clazz job data class
     * @return job executor
     */
    JobExecutorBuilder getBuilder(Class<? extends JobExecutor> clazz);
}
