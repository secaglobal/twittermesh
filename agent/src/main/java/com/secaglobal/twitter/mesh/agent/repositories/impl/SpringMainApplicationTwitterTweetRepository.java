package com.secaglobal.twitter.mesh.agent.repositories.impl;

import com.secaglobal.twitter.mesh.common.controllers.internal.request.RefreshTweetsForm;
import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.agent.repositories.MainApplicationTwitterTweetRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.LinkedList;

/**
 * Default Client Server Repository
 *
 * <p>
 *     This class provides interface client server tweets repository.
 *     Client server is a server responsible for web tweets representation
 *     This is a default implementation.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 18.01.2015
 */
public class SpringMainApplicationTwitterTweetRepository implements MainApplicationTwitterTweetRepository {
    private final Logger logger = LoggerFactory.getLogger(SpringMainApplicationTwitterTweetRepository.class);
    private final RestTemplate rest;
    private final String mainApplicationHttpUrl;

    private static final String PUSH_TWEETS = "/internal/agent/tweet";


    /**
     * Constructor
     * @param rest Api for initialization rest requests
     */
    public SpringMainApplicationTwitterTweetRepository(RestTemplate rest, String mainApplicationHttpUrl) {
        this.rest = rest;
        this.mainApplicationHttpUrl = mainApplicationHttpUrl;
    }

    /**
     * Save tweets
     * @param tweets List of tweets
     *
     * TODO UnitTest
     */
    public void save(Iterable<TwitterTweet> tweets) {
        logger.debug("Saving tweets via main application server request");
        RefreshTweetsForm form = new RefreshTweetsForm();
        RefreshTweetsForm.Tweets tweetsList = new RefreshTweetsForm.Tweets();

        for (TwitterTweet tweet: tweets) {
            tweetsList.add(tweet);
        }

        form.setTweets(tweetsList);

        rest.postForLocation(mainApplicationHttpUrl + PUSH_TWEETS, form);
    }
}
