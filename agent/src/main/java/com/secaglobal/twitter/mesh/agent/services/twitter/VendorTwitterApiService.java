package com.secaglobal.twitter.mesh.agent.services.twitter;

/**
 * Official Twitter Api Service Interface
 *
 * <p>
 *     This service class provide interface that organise interaction with official twitter api
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 * @see TwitterService
 */
public interface VendorTwitterApiService extends TwitterService {}
