package com.secaglobal.twitter.mesh.agent.services.mainApplication;

import com.secaglobal.twitter.mesh.common.domains.Job;
import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;

import java.util.Collection;

/**
 * Main Application Service Interface
 *
 * <p>
 *     This service class provide interface that organise interaction with main application
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public interface MainApplicationService {
    /**
     * Send tweets to main application server
     * @param tweets
     */
    void sendTweets(Collection<TwitterTweet> tweets);

    /**
     * Get list of jobs that should be processed current Agent
     * @returns list of jobs
     */
    Iterable<Job> getJobs();
}
