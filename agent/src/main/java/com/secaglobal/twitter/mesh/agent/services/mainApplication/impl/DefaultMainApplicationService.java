package com.secaglobal.twitter.mesh.agent.services.mainApplication.impl;

import com.secaglobal.twitter.mesh.agent.repositories.impl.MainAppJobRepository;
import com.secaglobal.twitter.mesh.common.domains.Job;
import com.secaglobal.twitter.mesh.common.domains.TwitterTweet;
import com.secaglobal.twitter.mesh.agent.repositories.MainApplicationTwitterTweetRepository;
import com.secaglobal.twitter.mesh.agent.services.mainApplication.MainApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

/**
 * Official Twitter Api Service Implementation
 *
 * <p>
 *     This service class provide interface that organise interaction with official twitter api
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 * @see com.secaglobal.twitter.mesh.agent.services.twitter.VendorTwitterApiService
 */
public class DefaultMainApplicationService implements MainApplicationService {
    private final Logger logger = LoggerFactory.getLogger(DefaultMainApplicationService.class);
    private MainApplicationTwitterTweetRepository tweetRepository;
    private MainAppJobRepository jobRepository;

    /**
     * Constructor
     * @param tweetRepository repository for tweets
     */
    public DefaultMainApplicationService(MainApplicationTwitterTweetRepository tweetRepository,
                                         MainAppJobRepository jobRepository) {
        this.tweetRepository = tweetRepository;
        this.jobRepository = jobRepository;
    }

    /**
     * Send tweets to main server
     * @param tweets
     */
    public void sendTweets(Collection<TwitterTweet> tweets) {
        logger.debug("Save tweets to main application repository");

        tweetRepository.save(tweets);
    }

    /**
     * Get list of jobs that should be processed current Agent
     * @returns list of jobs
     */
    public Iterable<Job> getJobs() {
        return jobRepository.findAllPending();
    }
}
