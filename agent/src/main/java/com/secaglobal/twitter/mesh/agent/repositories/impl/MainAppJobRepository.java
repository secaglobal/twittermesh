package com.secaglobal.twitter.mesh.agent.repositories.impl;

import com.secaglobal.twitter.mesh.agent.repositories.JobRepository;
import com.secaglobal.twitter.mesh.common.controllers.responses.impl.JobsDataResponse;
import com.secaglobal.twitter.mesh.common.domains.Job;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

/**
 * Main Application Server Repository interface
 *
 * <p>
 *     This class implements interface for getting jobs that should be processed by Agent.
 *     It retrieves jobs via MAin Application REST API
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 06.02.2015
 *
 * TODO UnitTest
 */
public class MainAppJobRepository implements JobRepository {
    private final Logger logger = LoggerFactory.getLogger(SpringMainApplicationTwitterTweetRepository.class);
    private final RestTemplate rest;
    private final String mainApplicationHttpUrl;

    private static final String PULL_JOBS = "/internal/agent/jobs";


    /**
     * Constructor
     * @param rest Api for initialization rest requests
     */
    public MainAppJobRepository(RestTemplate rest, String mainApplicationHttpUrl) {
        this.rest = rest;
        this.mainApplicationHttpUrl = mainApplicationHttpUrl;
    }

    /**
     * Returns list of jobs that should be executed by agent
     * @return list of jobs
     */
    public Iterable<Job> findAllPending() {
        logger.debug("Pulling jobs from application server");

        String url = mainApplicationHttpUrl + PULL_JOBS;
        return Arrays.asList(rest.getForEntity(url, JobsDataResponse.class).getBody().getData().jobs);
    }
}
