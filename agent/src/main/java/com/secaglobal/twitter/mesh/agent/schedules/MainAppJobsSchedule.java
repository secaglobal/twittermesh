package com.secaglobal.twitter.mesh.agent.schedules;

import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutor;
import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutorBuilder;
import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutorBuilderFactory;
import com.secaglobal.twitter.mesh.agent.lib.job.JobExecutorFactory;
import com.secaglobal.twitter.mesh.common.domains.Job;
import com.secaglobal.twitter.mesh.agent.services.mainApplication.MainApplicationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Tweets Proxy
 *
 * <p>
 *     Gets information (tweets, etc) from Twitter and send it to client side
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 17.01.2015
 */
public class MainAppJobsSchedule implements Runnable {
    private final Logger logger = LoggerFactory.getLogger(MainAppJobsSchedule.class);
    private MainApplicationService mainApplicationService;
    private JobExecutorFactory executorFactory;
    private JobExecutorBuilderFactory builderFactor;

    public MainAppJobsSchedule(MainApplicationService mainApplicationService,
                               JobExecutorFactory executorFactory,
                               JobExecutorBuilderFactory builderFactory) {

        this.mainApplicationService = mainApplicationService;
        this.executorFactory = executorFactory;
        this.builderFactor = builderFactory;
    }

    @SuppressWarnings("unchecked")
    public void run() {
        logger.info("Task started: {}", MainAppJobsSchedule.class.getSimpleName());

        Iterable<Job> jobs =  mainApplicationService.getJobs();

        logger.debug("Received jobs");

        ThreadPoolExecutor pool = new ThreadPoolExecutor(1, 100, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

        for (Job job: jobs) {
            logger.debug("Job starting: {}", job);
            Class<? extends JobExecutor> executorClass = executorFactory.getExecutor(job.getClass());
            JobExecutorBuilder builder = builderFactor.getBuilder(executorClass);
            JobExecutor executor = builder.build();
            executor.setData(job);
            pool.execute(executor);
        }

        logger.debug("All jobs was ran Awaiting completeness");

        try {
            pool.wait();
        } catch (InterruptedException e) {
            logger.error("Interruption in time of pool waiting", e);
        } finally {
            pool.shutdown();
        }

        logger.info("Task completed: {}", MainAppJobsSchedule.class.getSimpleName());
    }
}

