package com.secaglobal.twitter.mesh.agent.utils;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

/**
 * Utils for creating http clients
 *
 * <p>
 *     This class manage HttpClient instances building.
 * </p>
 *
 * @author Levandovskiy Sergey <levandovskiy.s@gmail.com>
 * @since 18.01.2015
 */
public class HttpClientFactory {

    /**
     * Creates simple http client
     * <p>This method creates http client using single user credentials for all requests</p>
     * TODO Unit Test
     * @param credentials http user credentials
     * @return http client
     * @throws Exception
     */
    public static CloseableHttpClient createSimple(Credentials credentials) throws Exception {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        HttpClientBuilder builder = HttpClientBuilder.create();

        credentialsProvider.setCredentials(AuthScope.ANY, credentials);
        builder.setDefaultCredentialsProvider(credentialsProvider);

        return builder.build();
    }
}
